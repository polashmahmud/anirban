<?php

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::post('signin', 'Auth\SigninController@signin');
Route::post('signup', 'Auth\SignupController@signup');

Route::get('/home', 'HomeController@index')->name('home');

// Calendar
Route::get('calendar', 'CalendarController@index');

// Send SMS
Route::post('sms/sendsms', 'SendsmsController@sendsms');
Route::post('sms/sendlonesms', 'SendsmsController@sendlonesms');

// View User Only
Route::get('/dashboard/show/{account}', 'DashboardController@show')->name('dashboard.show');
Route::post('/dashboard/password', 'DashboardController@password')->name('dashboard.password');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

// Member Route
Route::get('member/user/{user}', 'MemberController@active');
Route::post('member/profile/{profile}', 'MemberController@profile');
Route::resource('member', 'MemberController');

// Accounts Route
Route::get('account/lone/{account}', 'AccountController@active');
Route::resource('account', 'AccountController');

// Tasks Route
Route::get('task/user/{task}', 'TaskController@active');
Route::resource('task', 'TaskController');

// Others Route
Route::get('other/user/{other}', 'OtherController@active');
Route::resource('other', 'OtherController');

// Amounts Controller
Route::get('amount/details/{id}', 'AmountController@amountdetails');
Route::get('amount/daly/{account}', 'AmountController@amount');
Route::get('amount/saving/{account}', 'AmountController@saving');
Route::get('amount/dashboard', 'AmountController@dashboard');
Route::post('amount/daly', 'AmountController@dayamount');
Route::post('amount/transfer', 'AmountController@transfer');
Route::resource('amount', 'AmountController');

//Saving Route
Route::get('saving', 'SavingController@index');

// Lone Route
Route::get('lone', 'LoneController@index');
Route::get('lone/{id}', 'LoneController@show');
Route::post('lone/amount', 'LoneController@amount');
Route::post('lone/fixed', 'LoneController@fixed');

// Fixed Route
Route::get('fixed', 'FixedController@index');

// Role Route
Route::post('role/savepermission', 'RoleController@savepermission');
Route::get('role/add', 'RoleController@add');
Route::resource('role', 'RoleController');

// Permission Route
Route::resource('permission', 'PermissionController');

// Invoice Controller
Route::get('invoic/account-info/{account}', 'InvoiceController@accountinfo');
Route::get('invoic/account-info/print/{account}', 'InvoiceController@accountinfoprint');
Route::get('invoic/account-form/{account}', 'InvoiceController@accountform');

// Mobile
Route::get('/mobile/login', function () {
	if (Auth::check()) {
		return redirect('mobile/dashboard');
	} else {
    	return view('mobile.login');
	}
});
Route::get('mobile/dashboard', 'MobileController@dashboard');
Route::get('mobile/lone', 'MobileController@lone');
Route::get('mobile/saving', 'MobileController@saving');
Route::get('mobile/fixed', 'MobileController@fixed');
Route::get('mobile/show/{id}', 'MobileController@show');
Route::get('mobile/payment/{id}', 'MobileController@payment');
Route::post('mobile/login', 'MobileController@login');
