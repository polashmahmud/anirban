<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>সমিতিটি শুরু হয়েছেঃ</b> {{ \App\Classes\Helper::getDiffForHumans('2017-10-15') }}
    </div>
    <strong>অনির্বাণ সঞ্চয় ও ঋণদান সমিতি &copy; ২০১৭ {{-- <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved. --}}
  </footer>