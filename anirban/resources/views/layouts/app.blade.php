<!DOCTYPE html>
<html>
<head>
    @include('layouts.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('layouts.header')
  @include('layouts.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
    <h1>@yield('content-title')
      <small>@yield('content-subtitle')</small>
    </h1>
    <ol class="breadcrumb">
      @section('link-list')
        @show
    </ol>
    @include('layouts.flash-message')
  </section>

    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  @include('layouts.footer')
  @include('layouts.control-sidebar-menu')

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
@include('layouts.footer-script')
@include('flashy::message')
</body>
</html>

