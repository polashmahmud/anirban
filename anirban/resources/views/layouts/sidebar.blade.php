{{-- Left side column. contains the sidebar --}}
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        @if(isset(Auth::user()->profile->photo))
        <img src={{ asset(Auth::user()->profile->photo) }} class="img-circle" alt="User Image">
        @else
        <img src={{ asset("admin/dist/img/user2-160x160.jpg") }} class="img-circle" alt="User Image">
        @endif
      </div>
      <div class="pull-left info">
        <p>{{ \App\Classes\Helper::getName(\Auth::id()) }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> {{ \App\Classes\Helper::getAnirbanCode(\Auth::id()) }}</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      @can('dashboard.view', Auth::user())
      <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> <span>ডেসবোর্ড</span></a></li>
      @endcan
      @can('amount.view')
      <li><a href="{{ url('amount/dashboard') }}"><i class="fa fa-dollar"></i> <span>কালেকশন ডিটেলস</span></a></li>
      @endcan
      <li><a href="{{ url('calendar') }}"><i class="fa fa-calendar"></i> <span>ক‍্যালেন্ডার</span></a></li>
      @can('accounts.view', Auth::user())
      <li class="treeview">
        <a href="#">
          <i class="fa fa-user-plus"></i> <span>একাউন্ট সমূহ</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('lone') }}"><i class="fa  fa-heartbeat"></i> ঋণ একাউন্ট</a></li>
          <li><a href="{{ url('saving') }}"><i class="fa fa-heart"></i> সঞ্চয় একাউন্ট</a></li>
          <li><a href="{{ url('fixed') }}"><i class="fa  fa-heart-o"></i> ফিক্সড একাউন্ট</a></li>
        </ul>
      </li>
      @endcan
      @can('users.view', Auth::user())
      <li class="treeview">
        <a href="#">
          <i class="fa fa-user-secret"></i> <span>সদস‍্য সমূহ</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can('users.create', Auth::user())
          <li><a href="{{ route('member.create') }}"><i class="fa fa-plus"></i> নতুন যুক্ত করুন</a></li>
          @endcan
          <li><a href="{{ route('member.index') }}"><i class="fa fa-list"></i> সদস‍্য সমূহ</a></li>
        </ul>
      </li>
      @endcan
      @can('lone.view', Auth::user())
      <li class="treeview">
        <a href="#">
          <i class="fa fa-group"></i> <span>সঞ্চয় ও ঋণ সমূহ</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can('lone.create', Auth::user())
          <li><a href="{{ route('account.create') }}"><i class="fa fa-plus"></i> নতুন যুক্ত করুন</a></li>
          @endcan
          <li><a href="{{ route('account.index') }}"><i class="fa fa-list"></i> সঞ্চয় ও ঋণ সমূহ</a></li>
        </ul>
      </li>
      @endcan
      @can('other.view', Auth::user())
      <li class="treeview">
        <a href="#">
          <i class="fa fa-book"></i> <span>অন‍্যান‍্য</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can('other.create', Auth::user())
          <li><a href="{{ route('other.create') }}"><i class="fa fa-plus"></i> নতুন যুক্ত করুন</a></li>
          @endcan
          <li><a href="{{ route('other.index') }}"><i class="fa fa-list"></i> অন‍্যান‍্য সমূহ</a></li>
        </ul>
      </li>
      @endcan
      @can('amount.view', Auth::user())
      <li class="treeview">
        <a href="#">
          <i class="fa fa-money"></i> <span>টাকা সমূহ</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can('amount.create', Auth::user())
          <li><a href="{{ route('amount.create') }}"><i class="fa fa-plus"></i> নতুন যুক্ত করুন</a></li>
          @endcan
          <li><a href="{{ route('amount.index') }}"><i class="fa fa-list"></i> টাকা সমূহ</a></li>
        </ul>
      </li>
      @endcan
      @can('task.view', Auth::user())
      <li class="treeview">
        <a href="#">
          <i class="fa fa-tasks"></i> <span>Tasks</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can('task.create', Auth::user())
          <li><a href="{{ route('task.create') }}"><i class="fa fa-plus"></i> Add New</a></li>
          @endcan
          <li><a href="{{ route('task.index') }}"><i class="fa fa-list"></i> List Task</a></li>
        </ul>
      </li>
      @endcan
      @can('role.view')
      <li class="treeview">
        <a href="#">
          <i class="fa fa-lock"></i> <span>User Role</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('role/add') }}"><i class="fa fa-plus"></i> Role Add</a></li>
          <li><a href="{{ route('role.index') }}"><i class="fa fa-plus"></i> Role</a></li>
          <li><a href="{{ route('permission.index') }}"><i class="fa fa-list"></i> Permission</a></li>
        </ul>
      </li>
      @endcan
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

  <!-- ===============================================