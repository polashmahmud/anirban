@extends('layouts.app')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Invoice
    <small>#AAN-{{ $account->id }}</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Invoice</li>
  </ol>
</section>

{{-- <div class="pad margin no-print">
  <div class="callout callout-info" style="margin-bottom: 0!important;">
    <h4><i class="fa fa-info"></i> Note:</h4>
    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
  </div>
</div> --}}


<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        {{-- <i class="fa fa-pdf"></i> --}} অনির্বাণ সঞ্চয় ও ঋণদান সমিতি
        <small class="pull-right">তারিখঃ <span>{{ \Carbon\Carbon::now()->toFormattedDateString() }}</span></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      প্রেরক
      <address>
        <strong>অনির্বাণ সঞ্চয় ও ঋণ দান সমিতি</strong><br>
        ভোগড়া মধ‍্যপাড়া, বাইপাস রোড<br>
        জাতীয় বিশ্ববিদ‍্যালয়, জয়দেবপুর, গাজীপুর।<br>
        ফোন: (+৮৮) ০১৬৭৮-১২৬০৮৬<br>
        ইমেইল: polashmahmud@gmail.com
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      প্রাপক
      <address>
        <strong>{{ $account->user->name }}</strong><br>
        {{ $account->user->profile->present_address }}<br>
        ফোনঃ <span>{{ $account->user->profile->contact_number }}</span><br>
        সদস‍্য নংঃ AnairBan-{{ $account->user->id }}
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <b>Invoice #AAN-{{ $account->id }}</b><br>
      <br>
      <b>একাউন্ট শুরুর তারিখঃ</b> <span>{{ \App\Classes\Helper::getDate($account->date_of_account) }}</span><br>
      <b>ঋণের পরিমানঃ</b> <span>{{ \App\Classes\Helper::getNumber($account->lone_amount) }}</span> টাকা<br>
      {{-- <b>Account:</b> 968-34567 --}}
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>তারিখ</th>
          <th>সঞ্চয়</th>
          <th>ঋণ জমা</th>
          <th>উত্তোলন</th>
          <th>বোনাস</th>
        </tr>
        </thead>
        <tbody>
        @foreach($account->amount as $data)
          <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ \App\Classes\Helper::getDate($data->amount_date) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->saving_amount) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->lone_amount) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->elevation_amount) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->bonus_amount) }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <!-- accepted payments column -->
    <div class="col-xs-6">
      <p class="lead">আমাদের কথাঃ</p>
      {{-- <img src={{ asset("admin/dist/img/credit/visa.png") }} alt="Visa">
      <img src={{ asset("admin/dist/img/credit/mastercard.png") }} alt="Mastercard">
      <img src={{ asset("admin/dist/img/credit/american-express.png") }} alt="American Express">
      <img src={{ asset("admin/dist/img/credit/paypal2.png") }} alt="Paypal"> --}}

      <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
        আপনি আমাদের একজন মূল‍্যবান সদস‍্য। আশা করছি আমাদের সাথে আপনার সময়টা ভালোই কেটেছে। আমাদের প্রতিষ্ঠানটি সম্পূর্ণ সুদ মুক্ত কিন্তু আমাদের এই প্রতিষ্ঠান চালানোর জন‍্য সামান‍্য কিছু লভ‍্যাংশ আপনাদের কাছ থেকে আদায় করা হয়ে থাকে যার মাধ‍্যমে আমরা আপনাদের বিভিন্ন ধরনের সুযোগ সুবিধা প্রদান করে থাকি। আপনি আপনার প্রতিষ্ঠানে বিনিয়োগের জন‍্য আমাদের কাছ থেকে <span>{{\App\Classes\Helper::getNumber($account->lone_amount)}}</span> টাকা নিয়েছিলেন এবং আপনি তা <span>{{\App\Classes\Helper::getDiffInDays($account->date_of_account) }}</span> দিনে পরিশোধ করেছেন। আপনাকে অভিনন্দন। ঋণ <span>{{\App\Classes\Helper::getNumber($account->lone_amount)}}</span> টাকা বাবদ আমরা লাভ নিয়েছি <span>{{\App\Classes\Helper::getNumber($account->lone_amount / 10)}}</span> টাকা। আপনি যদি মনে করেন এই লভ‍্যাংশ প্রদান করা আপনার জন‍্য কষ্টকর অথবা আমাদের কাছ থেকে টাকা নিয়ে আপনার প্রতিষ্ঠানে কোন প্রকার লাভ হয় নি। তাহলে আপনি আমাদের সাথে যোগাযোগ করতে পারেন। আমরা আলাপ আলোচনার মাধ‍্যমে আপনার কাছ থেকে গ্রহনকৃত লভ‍্যাংশ প্রদান করে দিবো আপনাকে।
      </p>
    </div>
    <!-- /.col -->
    <div class="col-xs-6">
      <p class="lead">বিস্তারিত হিসাবঃ</p>

      <div class="table-responsive">
        <table class="table">
          <tr>
            <th style="width:70%">আপনি ঋণ নিয়েছেনঃ</th>
            <td>{{  \App\Classes\Helper::getNumber($account->lone_amount)  }}</td>
          </tr>
          <tr>
            <th>আপনি ঋণের টাকা জমা দিয়েছেনঃ</th>
            <td>{{  \App\Classes\Helper::getNumber($account->amount->sum('lone_amount'))  }}</td>
          </tr>
          <tr>
            <th>আপনি সঞ্চয় করেছেনঃ</th>
            <td>{{  \App\Classes\Helper::getNumber($account->amount->sum('saving_amount'))  }}</td>
          </tr>
          <tr>
            <th>আপনি টাকা উত্তোলন করেছেনঃ</th>
            <td>{{  \App\Classes\Helper::getNumber($account->amount->sum('elevation_amount'))  }}</td>
          </tr>
          <tr>
            <th>আমাদের তরফ থেকে বোনাস প্রদানঃ</th>
            <td>{{  \App\Classes\Helper::getNumber($account->amount->sum('bonus_amount'))  }}</td>
          </tr>
          <tr>
            <th>আপনি {{ \App\Classes\Helper::getNumber($account->lone_amount) }} টাকায় সমিতিতে লাভ প্রদান করেছেনঃ</th>
            <td>{{ \App\Classes\Helper::getNumber($account->lone_amount / 10) }}</td>
          </tr>
          <tr>
            <th>একাউন্ট স্থতিত ফরম বাবদঃ</th>
            <td>৫০</td>
          </tr>
          <tr>
            <th>সর্বমোটঃ</th>
            <td>{{ \App\Classes\Helper::getNumber(($account->amount->sum('lone_amount') + $account->amount->sum('saving_amount') + $account->amount->sum('bonus_amount')) - (($account->lone_amount + $account->amount->sum('elevation_amount') + ($account->lone_amount / 10) + 50))) }}</td>
          </tr>
        </table>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="{{ url('invoic/account-info/print', $account->id) }}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
      {{-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
      </button>
      <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
        <i class="fa fa-download"></i> Generate PDF
      </button> --}}
    </div>
  </div>
</section>
<!-- /.content -->
<div class="clearfix"></div>
@endsection

@section('footer-script')
  @include('invoice.bangla')
@endsection



