@extends('layouts.app')
@section('title', 'Account Form')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Invoice
    <small>#AAN-{{ $account->id }}</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Invoice</li>
  </ol>
</section>

{{-- <div class="pad margin no-print">
  <div class="callout callout-info" style="margin-bottom: 0!important;">
    <h4><i class="fa fa-info"></i> Note:</h4>
    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
  </div>
</div> --}}


<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        {{-- <i class="fa fa-pdf"></i> --}} অনির্বাণ সঞ্চয় ও ঋণদান সমিতি (ঋণ ফরম)
        <small class="pull-right">তারিখঃ <span>{{ \Carbon\Carbon::now()->toFormattedDateString() }}</span></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      প্রথম পক্ষ
      <address>
        <strong>অনির্বাণ সঞ্চয় ও ঋণ দান সমিতি</strong><br>
        ভোগড়া মধ‍্যপাড়া, বাইপাস রোড<br>
        জাতীয় বিশ্ববিদ‍্যালয়, জয়দেবপুর, গাজীপুর।<br>
        ফোন: (+৮৮) ০১৬৭৮-১২৬০৮৬<br>
        ইমেইল: polashmahmud@gmail.com
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      দ্বিতীয় পক্ষ
      <address>
        <strong>{{ $account->user->name }}</strong><br>
        {{ $account->user->profile->present_address }}<br>
        ফোনঃ <span>{{ $account->user->profile->contact_number }}</span><br>
        সদস‍্য নংঃ AnairBan-{{ $account->user->id }}
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <b>একাউন্ট নম্বরঃ #AAN-{{ $account->id }}</b><br>
      <br>
      <b>একাউন্ট শুরুর তারিখঃ</b> <span>{{ \App\Classes\Helper::getDate($account->date_of_account) }}</span><br>
      <b>ঋণের পরিমানঃ</b> <span>{{ \App\Classes\Helper::getNumber($account->lone_amount) }}</span> টাকা<br>
      {{-- <b>Account:</b> 968-34567 --}}
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <br>
  <p style="margin-top: 30px;">আমি নিম্মে বর্ণিত সকল প্রকার শর্তাবলী ও নিয়মকানুনের সাথে একমত হয়ে <strong>“অনির্বাণ সঞ্চয় ও ঋণদান সমিতি”</strong> হতে <strong><span>{{ \App\Classes\Helper::getNumber($account->lone_amount) }}</span></strong> টাকা আমার ব‍্যবসার মূলধনে বিনিয়োগ করবার জন‍্য উত্তোলন করছি এবং আমি এই উত্তোলনকৃত টাকা পরিশোধ করবার জন‍্য প্রতিদিন <strong><span>{{ \App\Classes\Helper::getNumber($account->lone_amount / 100) }}</span></strong> টাকা সমিতিতে <strong><span>{{ 100 }}</span></strong> দিন জমা করিয়া আমার ঋণ পরিশোধ করিবো। যদি আমার ব‍্যবসায় বিনিয়োগকৃত টাকা কতৃক লাভ হয় তাহলে আমি খুশি হয়ে সমিতিতে <strong><span>{{ \App\Classes\Helper::getNumber(($account->lone_amount * 10) / 100) }}</span></strong> টাকা প্রদান করিবো যা আমার সঞ্চয় হতে কর্তন করা হইবে অথবা আমার ব‍্যবসায় যদি লস হয় এবং এই <strong><span>{{ \App\Classes\Helper::getNumber(($account->lone_amount * 10) / 100) }}</span></strong> টাকা সমিতিতে লাভ হিসাবে দিতে আমি যদি অক্ষম হই তাহলে আমি সমিতির কর্তৃপক্ষকে জানাবো যাতে করে উনি আমার কাছ থেকে লাভের পরিমান কম কর্তন করেন। যদি কোন প্রকার লস এর সম্মুক্ষিন হই তাহলে আমি সমিতির পরিচিলনা বিভাগে যতো দ্রুত সম্ভব জানাবো যাতে তারা তাদের পদক্ষেপ নিতে পারেন। আমি প্রতিদিন উত্তোলনকৃত টাকা <strong><span>{{ \App\Classes\Helper::getNumber($account->lone_amount / 100) }}</span></strong> করিয়া পরিশোধ করবার পাশাপাশি <strong><span>{{ $account->saving_amount }}</span></strong> টাকা করিয়া সঞ্চয় জমা করিবো।
</p>
<br>
<div class="row">
  <div class="col-md-6">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">শর্তাবলীঃ</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <ul>
          <li>টাকা জমা দেওয়ার সময় পাস বইতে কর্মীর স্বাক্ষর অবশ‍্যই নিতে হবে।</li>
          <li>পাসবইতে লিপিবদ্ধ নেই এমন হিসাব গ্রহনযোগ‍্য হবে না।</li>
          <li>কোন কারনে ঋণ পাশ বই হারিয়ে বা নষ্ট হয়ে গেলে তৎক্ষনাৎ অফিসে লিখিতভাবে জানাতে হবে এবং নতুন বই ৫০/- টাকা জমা দিয়ে সংগ্রহ করতে হবে।</li>
          <li>কিন্তি খেলাপী হলে খেলাপী কিস্তির উপর ১০% হারে সার্ভিস চার্জ ধার্য ও আদায় করা হবে।</li>
          <li>এ সংস্থা যে কোন সময় যে কোন নিয়মাবলি সংশোধন, পরিবর্তন , পরিবর্ধন বা বাতিল করার অধিকার সংরক্ষন করে।</li>
        </ul>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-6">
    <div class="signature pull-right" style="margin-top: 40px; margin-right: 100px;">
      <h5>স্বাক্ষর</h5>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12" style="margin-top: 20%;">
    <hr>
    <h5 class="text-center">“আজকের সঞ্চয় আগামী দিনের ভতিষ্যৎ”</h5>
  </div>
</div>

  
</section>
<!-- /.content -->
<div class="clearfix"></div>
@endsection

@section('footer-script')
  @include('invoice.bangla')
@endsection



