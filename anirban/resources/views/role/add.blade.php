@extends('layouts.app')

@section('title', 'Role Add')
@section('content-title', 'Role Add')
@section('content-subtitle', 'All Role Add')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('role.index') }}">Role</a></li>
@endsection


@section('content')
@can('role.view')
<section class="content">
<div class="box">
  <div class="box-header">
    @if(Auth::user()->roles('edit'))
    <h3 class="box-title">Responsive Hover Table</h3>
    @endif

    {{-- <div class="box-tools">
      <div class="input-group input-group-sm" style="width: 150px;">
        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

        <div class="input-group-btn">
          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </div>
      </div>
    </div> --}}
  </div>
  <!-- /.box-header -->
  <div class="box-body table-responsive no-padding">
    <form action="{{ url('role/savepermission') }}" method="post">
      {{ csrf_field() }}
    <table class="table table-hover">
      <tbody><tr>
        <th>Permission</th>
        @foreach($roles as $role)
        <th>{{ $role->name }}</th>
        @endforeach
      </tr>
      <tr>
        @foreach($permissions as $permission)
        <td>{{ $permission->name }} - 
          @if($permission->for == 1) ডেসবোর্ড
          @elseif($permission->for == 2) একাউন্টস সমূহ
          @elseif($permission->for == 3) সদস‍্য সমূহ
          @elseif($permission->for == 4) সঞ্চয় ও ঋণ সমূহ
          @elseif($permission->for == 5) অন‍্যান‍্য
          @elseif($permission->for == 6) টাকা সমূহ
          @elseif($permission->for == 7) Tasks
          @elseif($permission->for == 8) User Role
          @else
          Muri Khan
          @endif
        </td>
        @foreach($roles as $role)
          <th><input type="checkbox" name="permission[{!!$role->id!!}][{!!$permission->id!!}]" value = '1' @foreach($role->permissions as $role_permit) 
            @if($role_permit->id == $permission->id)
              checked
            @endif
          @endforeach  
          </th>
        @endforeach
      </tr>
      @endforeach
      </tr>
    </tbody>
  </table>
  </div>
  <div class="box-footer clearfix">
      <button class="pull-right btn btn-default" type="submit">Add Permission</button>
  </div>
  </form>
  <!-- /.box-body -->
</div>

</section>
@endcan
@endsection

