@extends('layouts.app')

@section('title', 'Role')
@section('content-title', 'Role')
@section('content-subtitle', 'All Role')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('role.index') }}">Role</a></li>
@endsection


@section('content')
@can('role.view', Auth::user())
<section class="content">
<!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      @can('role.create', Auth::user())
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Role Create From</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('role.store') }}" method="post">
          {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label>Role Name</label>
              <input type="text" class="form-control"  name="name" placeholder="Role Name">
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Add New Role</button>
          </div>
        </form>
      </div>
      @endcan
    </div>
    <div class="col-md-6">
      @if($roles->isNotEmpty())
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">All Role</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-striped">
            <tbody><tr>
              <th style="width: 10px">#</th>
              <th>Role Name</th>
              <th style="width: 100px"></th>
            </tr>
            @foreach($roles as $role)
            <tr>
              <td>{{ $loop->index + 1 }}</td>
              <td>{{ $role->name }}</td>
              <td>
                @can('role.update')
                <a href="{{ route('role.edit', $role->id) }}" class="btn btn-info btn-xs"><span class="fa fa-edit"></span></a>
                @endcan
                @can('role.delete')
              <form id="delete-form-{{ $role->id }}" method="post" action="{{ route('role.destroy',$role->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
              <a class="btn btn-danger btn-xs" href="" onclick="
              if(confirm('Are you sure, You Want to delete this?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $role->id }}').submit();
                  }
                  else{
                    event.preventDefault();
                  }" ><span class="fa fa-trash"></span></a>
                  @endcan
              </td>
            </tr>
            @endforeach
          </tbody></table>
        </div>
        <!-- /.box-body -->
      </div>
      @endif
    </div>
  </div>
  <!-- /.box -->
</section>
@endcan
@endsection

