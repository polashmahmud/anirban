@extends('layouts.app')

@section('title', 'Role')
@section('content-title', 'Role')
@section('content-subtitle', 'All Role')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('role.index') }}">Role</a></li>
@endsection


@section('content')
@can('role.view', Auth::user())
<section class="content">
<!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Role Create From</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('role.update', $role->id) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="box-body">
            <div class="form-group">
              <label>Role Name</label>
              <input type="text" class="form-control"  name="name" placeholder="Role Name" value="{{ $role->name }}">
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Edit Role</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      
    </div>
  </div>
  <!-- /.box -->

</section>
@endcan
@endsection

