@extends('layouts.app')

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/select2/dist/css/select2.min.css") }}>
@endsection

@section('title', 'সঞ্চয় ও ঋন জমা')
@section('content-title', 'সঞ্চয় ও ঋন জমা')
@section('content-subtitle', 'সঞ্চয় ও ঋন জমা ফরম')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li ><a href="{{ route('amount.index') }}">সঞ্চয় ও ঋন জমা লিস্ট</a></li>
<li class="active"><a href="{{ route('amount.create') }}">সঞ্চয় ও ঋন জমা ফরম</a></li>
@endsection

@section('content')
@can('amount.create', Auth::user())
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">সঞ্চয় ও ঋন জমা ফরম</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('amount.store') }}" method="post">
          {{ csrf_field() }}
          <div class="box-body">

            <div class="form-group">
              <label>একাউন্ট লিস্ট</label>
              <select class="form-control select2" style="width: 100%;" name="account_id">
                @foreach($accounts as $account)
                <option value="{{ $account->id }}">AAN-{{ $account->id }} - {{ $account->user->name }} , {{ $account->user->profile->contact_number }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="name">তারিখ</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" id="datepicker" name="amount_date" data-date-format="yyyy/mm/dd">
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="saving_amount">সঞ্চয়</label>
                  <input class="form-control" id="saving_amount" placeholder="Saving Amount" type="number" name="saving_amount" value="{{ old('saving_amount') }}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="lone_amount">ঋণ</label>
                  <input class="form-control" id="lone_amount" placeholder="Lone Amount" type="number" name="lone_amount" value="{{ old('lone_amount') }}">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="elevation_amount">উত্তোলন</label>
                  <input class="form-control" id="elevation_amount" placeholder="Elevation Amount" type="number" name="elevation_amount" value="{{ old('elevation_amount') }}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="bonus_amount">বোনাস</label>
                  <input class="form-control" id="bonus_amount" placeholder="Bonus Amount" type="number" name="bonus_amount" value="{{ old('bonus_amount') }}">
                </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">যুক্ত করুন</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ব‍্যবহার বিধি</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            
          </div>
      </div>
    </div>
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/moment/min/moment.min.js") }}></script>
<script src={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}></script>
<!-- bootstrap datepicker -->
<script src={{ asset("admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}></script>
<script src={{ asset("admin/bower_components/select2/dist/js/select2.full.min.js") }}></script>

<script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
    $('.select2').select2()
  })
</script>
@endsection
