@extends('layouts.app')

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/select2/dist/css/select2.min.css") }}>
@endsection

@section('title', 'Amount Page')
@section('content-title', 'সঞ্চয়')
@section('content-subtitle', 'সমিতির সঞ্চয় ফরম')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li ><a href="{{ route('amount.index') }}">Amount list</a></li>
<li class="active"><a href="{{ route('amount.create') }}">Amount create</a></li>
@endsection

@section('content')
@can('amount.create', Auth::user())
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">সঞ্চয় ফরম</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ url('amount/daly') }}" method="post">
          {{ csrf_field() }}
          <div class="box-body">

            <input type="text" name="account_id" value="{{ $account->id }}" hidden="">
            <input type="text" name="lone_amount" value="0" hidden="">

            <div class="form-group">
              <label for="name">Amount Date</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" id="datepicker" name="amount_date" data-date-format="yyyy/mm/dd">
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="saving_amount">Saving Amount</label>
                  <input class="form-control" id="saving_amount" placeholder="Saving Amount" type="number" name="saving_amount" value="{{ $account->saving_amount }}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="lone_amount">Lone Amount</label>
                  <input class="form-control" id="lone_amount" placeholder="Lone Amount" type="number" name="lone_amount" value="0" disabled>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="elevation_amount">Elevation Amount</label>
                  <input class="form-control" id="elevation_amount" placeholder="Elevation Amount" type="number" name="elevation_amount" value="0">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="bonus_amount">Bonus Amount</label>
                  <input class="form-control" id="bonus_amount" placeholder="Bonus Amount" type="number" name="bonus_amount" value="0">
                </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Add New Account</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-yellow">
          <div class="widget-user-image">
            @if(isset($account->user->profile->photo))
            <img src={{ asset($account->user->profile->photo) }} class="img-circle" alt="User Avatar">
            @else
            <img class="img-circle" src={{ asset("admin/dist/img/user7-128x128.jpg") }} alt="User Avatar">
            @endif
            
          </div>
          <!-- /.widget-user-image -->
          <h3 class="widget-user-username">{{ $account->user->name }}</h3>
          <h5 class="widget-user-desc">@if($account->account_type == 0) Lone Account @else Saving Acount @endif</h5>
        </div>
        <div class="box-footer no-padding">
          <ul class="nav nav-stacked">
            <li><a href="#">হিসাব নং <span class="pull-right badge bg-blue">AAN-{{ $account->id }}</span></a></li>
            {{-- <li><a href="#">ঋণের পরিমান <span class="pull-right badge bg-aqua">{{ $account->lone_total_amount }}</span></a></li> --}}
            <li><a href="#">সঞ্চয় শুরুর তারিখ <span class="pull-right badge bg-green">{{ $account->date_of_account }}</span></a></li>
            <li><a href="#">সঞ্চয় এর পরিমান <span class="pull-right badge bg-red">{{ $account->saving_amount }}</span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/moment/min/moment.min.js") }}></script>
<script src={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}></script>
<!-- bootstrap datepicker -->
<script src={{ asset("admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}></script>
<script src={{ asset("admin/bower_components/select2/dist/js/select2.full.min.js") }}></script>

<script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      multidate: true,
      todayHighlight: true
    })
    $('.select2').select2()
  })
</script>
@endsection
