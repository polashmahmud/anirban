@extends('layouts.app')

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/select2/dist/css/select2.min.css") }}>
@endsection

@section('title', 'Amount Page')
@section('content-title', 'Amount Page')
@section('content-subtitle', 'Amount Page')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li ><a href="{{ route('amount.index') }}">Amount list</a></li>
<li class="active"><a href="{{ route('amount.create') }}">Amount create</a></li>
@endsection

@section('content')
@can('amount.update', Auth::user())
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Quick Example</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('amount.update', $amount->id) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="box-body">

            <div class="form-group">
              <label for="account_no">Account No</label>
              <input class="form-control" id="account_no" type="test" value="AAN-{{ $amount->account->id }}" disabled>
            </div>

            <div class="form-group">
              <label for="name">Amount Date</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" id="datepicker" name="amount_date" data-date-format="yyyy/mm/dd" value="{{ $amount->amount_date }}">
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="saving_amount">Saving Amount</label>
                  <input class="form-control" id="saving_amount" placeholder="Saving Amount" type="number" name="saving_amount" value="{{ $amount->saving_amount }}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="lone_amount">Lone Amount</label>
                  <input class="form-control" id="lone_amount" placeholder="Lone Amount" type="number" name="lone_amount" value="{{ $amount->lone_amount }}">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="elevation_amount">Elevation Amount</label>
                  <input class="form-control" id="elevation_amount" placeholder="Elevation Amount" type="number" name="elevation_amount" value="{{ $amount->elevation_amount }}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="bonus_amount">Bonus Amount</label>
                  <input class="form-control" id="bonus_amount" placeholder="Bonus Amount" type="number" name="bonus_amount" value="{{ $amount->bonus_amount }}">
                </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Edit Amount</button>
            <a href="{{ route('account.show', $amount->account->id) }}" class="btn btn-default pull-right">Back</a>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Quick Example</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            
          </div>
      </div>
    </div>
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/moment/min/moment.min.js") }}></script>
<script src={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}></script>
<!-- bootstrap datepicker -->
<script src={{ asset("admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}></script>
<script src={{ asset("admin/bower_components/select2/dist/js/select2.full.min.js") }}></script>

<script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
    $('.select2').select2()
  })
</script>
@endsection
