@extends('layouts.app')

@section('title', 'সঞ্চয় ও জমা')
@section('content-title', 'সঞ্চয় ও জমা')
@section('content-subtitle', 'সঞ্চয় ও জমা লিস্ট')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('amount.index') }}">সঞ্চয় ও জমা লিস্ট</a></li>
@endsection

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}>
@endsection

@section('content')
@can('amount.view', Auth::user())
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header">
      {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>নাম</th>
            <th>তারিখ</th>
            <th>সঞ্চয়</th>
            <th>ঋণ</th>
            <th>উত্তোলন</th>
            <th>বোনাস</th>
            @can('amount.update', Auth::user())
            <th></th>
            @endcan
          </tr>
        </thead>
        <tbody>
          @foreach($amounts as $amount)
          <tr>
            <td>{{ $amount->account->user->name }}</td>
            <td>{{ \App\Classes\Helper::getDate($amount->amount_date) }}</td>
            <td>{{ $amount->saving_amount }}</td>
            <td>{{ $amount->lone_amount }}</td>
            <td>{{ $amount->elevation_amount }}</td>
            <td>{{ $amount->bonus_amount }}</td>
            @can('amount.update')
            <td>
              <a href="{{ route('amount.edit', $amount->id) }}" class="btn btn-info btn-xs"><span class="fa fa-edit"></span></a>

              @can('amount.delete')
              <form id="delete-form-{{ $amount->id }}" method="post" action="{{ route('amount.destroy',$amount->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
              <a class="btn btn-danger btn-xs" href="" onclick="
              if(confirm('Are you sure, You Want to delete this?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $amount->id }}').submit();
                  }
                  else{
                    event.preventDefault();
                  }" ><span class="fa fa-trash"></span></a>
              @endcan
            </td>
            @endcan
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>নাম</th>
            <th>তারিখ</th>
            <th>সঞ্চয়</th>
            <th>ঋণ</th>
            <th>উত্তোলন</th>
            <th>বোনাস</th>
            @can('amount.update')
            <th></th>
            @endcan
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/datatables.net/js/jquery.dataTables.min.js") }}></script>
<script src={{ asset("admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}></script>
<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>
@endsection