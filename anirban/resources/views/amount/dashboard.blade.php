@extends('layouts.app')

@section('title', 'Amount-Dashboard')
@section('content-title', 'Amount Dashboard')
@section('content-subtitle', 'অনির্বাণ সঞ্চয় ও ঋণদান সমিতি')
@section('link-list')
<li class="active"><a href="#"><i class="fa fa-dashboard"></i> Amount Dashboard</a></li>
@endsection

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/jvectormap/jquery-jvectormap.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}>
@endsection

@section('content')

@can('amount.view', Auth::user())

<!-- Main content -->
<section class="content">
 <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-money"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">আজ কালেকশন</span>
          <span class="info-box-number">{{ \App\classes\Helper::getNumber($today_amount->sum('saving_amount') + $today_amount->sum('lone_amount')) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-briefcase"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">গতকালের কালেকশন</span>
          <span class="info-box-number">{{ \App\classes\Helper::getNumber($yesterday_amount->sum('saving_amount') + $yesterday_amount->sum('lone_amount')) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-bank"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">ক‍্যাশে আছে</span>
          <span class="info-box-number">{{ \App\classes\Helper::getNumber($balance_amount - ($main_account_balance->amount_saving_amount + $main_account_balance->amount_lone_amount)) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa  fa-line-chart"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">ব‍্যালেন্স</span>
          <span class="info-box-number">{{ \App\classes\Helper::getNumber($balance_amount) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>

  <div class="row">
    @foreach($user_cash_balance as $row)
    <div class="col-md-4">
      <!-- Widget: user widget style 1 -->
      <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-white">
          <div class="widget-user-image">
            <img src={{ asset(\App\classes\Helper::getAvatar($row->user_id)) }} alt="User Avatar" class="img-circle" style="width: 65px; height: 65px;">
          </div>
          <!-- /.widget-user-image -->
          <h3 class="widget-user-username">{{ \App\classes\Helper::getName($row->user_id) }}</h3>
          <h5 class="widget-user-desc">TK: {{ \App\classes\Helper::getNumber($row->amount_saving_amount + $row->amount_lone_amount) }} 
            @can('amount.delete', Auth::user())
            @if($row->user_id != 1)
            <span>
              <form action="{{ url('amount/transfer') }}" method="post">
              {{ csrf_field() }}  
              <input type="text" value="{{ $row->user_id }}" name="user_id" hidden>
              <button type="submit" class="btn btn-default pull-right btn-xs">ট্রান্সফার</button>
              </form>
            </span>
            @endif
            @endif
          </h5>
        </div>
      </div>
      <!-- /.widget-user -->
    </div>
    @endforeach
    <!-- /.col -->
  </div>

  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">কালেকশন</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tbody><tr>
              <th>#</th>
              <th>সংগ্রহ করেছেন</th>
              <th>তারিখ</th>
              <th>একাউন্ট</th>
              <th>কিস্তি তারিখ</th>
              <th>সঞ্চয়</th>
              <th>ঋণ</th>
              <th>উঠানো</th>
              <th>বোনাস</th>
              <th></th>
            </tr>
            @foreach($all_amount as $row)
            <tr>
              <td>{{ $loop->index + 1 }}</td>
              <td>{{ \App\classes\Helper::getName($row->user_id) }}</td>
              <td>{{ \App\classes\Helper::getDate($row->created_at) }}</td>
              <td>{{ $row->account->user->name }} (AAN-{{ $row->account_id }})</td>
              <td>{{ \App\classes\Helper::getDate($row->amount_date) }}</td>
              <td>{{ $row->saving_amount }}</td>
              <td>{{ $row->lone_amount }}</td>
              <td>{{ $row->elevation_amount }}</td>
              <td>{{ $row->bonus_amount }}</td>
              <td>
              @can('amount.delete')
              <form id="delete-form-{{ $row->id }}" method="post" action="{{ route('amount.destroy',$row->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
              <a class="btn btn-danger btn-xs" href="" onclick="
              if(confirm('Are you sure, You Want to delete this?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $row->id }}').submit();
                  }
                  else{
                    event.preventDefault();
                  }" ><span class="fa fa-trash"></span></a>
              @endcan
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            {{ $all_amount->links() }}
          </ul>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>

  
</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js") }}></script>
<script src={{ asset("admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js") }}></script>
<script src={{ asset("admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js") }}></script>
<script src={{ asset("js/Chart.js") }}></script>
<script src={{ asset("admin/bower_components/datatables.net/js/jquery.dataTables.min.js") }}></script>
<script src={{ asset("admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}></script>
@endsection