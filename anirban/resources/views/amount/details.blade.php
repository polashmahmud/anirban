@extends('layouts.app')

@section('title', 'Amount page')
@section('content-title', 'Amount page')
@section('content-subtitle', 'Amount Page')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active"><a href="{{ route('amount.index') }}">Amount</a></li>
@endsection

@section('content')
@can('amount.view', Auth::user())
<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">সঞ্চয় ও ঋণ জমা হিসাব</h3>

      {{-- <div class="box-tools">
        <div class="input-group input-group-sm" style="width: 150px;">
          <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

          <div class="input-group-btn">
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
          </div>
        </div>
      </div> --}}
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
      <table class="table table-hover">
        <tbody><tr>
          <th>নাম</th>
          <th>তারিখ</th>
          <th>সঞ্চয়</th>
          <th>ঋণ</th>
          <th>উত্তোলন</th>
          <th>বোনাস</th>
          @can('amount.update', Auth::user())
          <th></th>
          @endcan
        </tr>
        @foreach($amounts as $amount)
        <tr>
          <td>{{ $amount->account->user->name }}</td>
          <td>{{ $amount->amount_date }}</td>
          <td>{{ $amount->saving_amount }}</td>
          <td>{{ $amount->lone_amount }}</td>
          <td>{{ $amount->elevation_amount }}</td>
          <td>{{ $amount->bonus_amount }}</td>
          @can('amount.update', Auth::user())
          <td>
            <a href="{{ route('amount.edit', $amount->id) }}" class="btn btn-info btn-xs"><span class="fa fa-edit"></span></a>
            @can('amount.delete', Auth::user())
            <form id="delete-form-{{ $amount->id }}" method="post" action="{{ route('amount.destroy',$amount->id) }}" style="display: none">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
            </form>
            <a class="btn btn-danger btn-xs" href="" onclick="
            if(confirm('Are you sure, You Want to delete this?'))
                {
                  event.preventDefault();
                  document.getElementById('delete-form-{{ $amount->id }}').submit();
                }
                else{
                  event.preventDefault();
                }" ><span class="fa fa-trash"></span></a>
              @endcan
          </td>
          @endcan
        </tr>
        @endforeach
      </tbody></table>
    </div>
    <!-- /.box-body -->
  </div>
</section>
<!-- /.content -->
@endcan
@endsection

