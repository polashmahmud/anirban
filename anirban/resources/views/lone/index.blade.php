@extends('layouts.app')

@section('title', 'ঋন একাউন্ট')
@section('content-title', 'ঋন')
@section('content-subtitle', 'সকল ঋন সমূহ')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ url('lone') }}">ঋণ</a></li>
@endsection


@section('content')
@can('accounts.view')
<!-- Main content -->
<section class="content">
  <div class="row">
    @foreach($lone_accounts as $account)
    <div class="col-md-4">
      <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header {{ \App\Classes\Helper::getProgress($account->id) }}">
          <div class="widget-user-image">
            @if(isset($account->user->profile->photo))
            <img src={{ asset($account->user->profile->photo) }} alt="User Avatar" class="img-circle" style="width: 65px; height: 65px;">
            @else
            <img class="img-circle" src="/admin/dist/img/user7-128x128.jpg" alt="User Avatar">
            @endif
          </div>
          <!-- /.widget-user-image -->
          <h3 class="widget-user-username">{{ $account->user->name }}</h3>
          <h5 class="widget-user-desc">সর্বশেষ পরিশোধ {{ \App\Classes\Helper::getLastAmountDate($account->id) }} / AAN-{{ $account->id }}</h5>
          <h5 class="widget-user-desc">
          <form action="{{ url('sms/sendlonesms') }}" method="post" style="display: inline-block;">
          {{ csrf_field() }}
          <input type="text" name="ac" value="AAN-{{ $account->id }}" hidden>
          <input type="text" name="user_id" value="{{ $account->user->id }}" hidden>
          <input type="text" name="kisty" value="{{ $account->amount_amount_date }}" hidden>
          <input type="text" name="amount" value="{{ ($account->amount_saving_amount + $account->amount_lone_amount) - $account->amount_elevation_amount}}" hidden>
          <input type="text" name="balance" value="{{ $account->lone_amount - ($account->amount_saving_amount + $account->amount_lone_amount)}}" hidden>
          <button type="submit" class="btn btn-default btn-xs"><i class="fa fa-send-o"></i> SMS</button>
          </form>
          </h5>
        </div>
        <div class="box-footer no-padding">
          <ul class="nav nav-stacked">
            <li><a href="{{ route('account.show', $account->id) }}">মোট কিস্তি <span class="pull-right"><strong>@if($account->month >= 1) {{ ($account->month) }} @elseif($account->week >= 1) {{ $account->week }} @elseif($account->day >= 1) {{ $account->day }} @else জানা নাই!!! @endif - {{ $account->amount_amount_date }} = {{ ($account->day + $account->week +$account->month) - ($account->amount_amount_date) }}</strong></span></a></li>
            <li><a href="#">ঋণের পরিমান <span class="pull-right"><strong>{{ \App\Classes\Helper::getNumber($account->lone_amount) }} - {{ \App\Classes\Helper::getNumber($account->amount_lone_amount) }} = {{ \App\Classes\Helper::getNumber($account->lone_amount - $account->amount_lone_amount) }}</strong></span></a></li>
            <li><a href="#">সঞ্চয় <span class="pull-right"><strong>{{ $account->saving_amount }} / {{ \App\Classes\Helper::getNumber((($account->amount_saving_amount + $account->amount_bonus_amount)) - $account->amount_elevation_amount) }}</strong></span></a></li>
            <li><a href="">বোনাস টাকার পরিমান <strong class="pull-right">{{ number_format(((($account->amount_saving_amount + $account->amount_bonus_amount) - $account->amount_elevation_amount) * 0.00055) * $account->amount_amount_date, 2) }} টাকা</strong></a></li>
            <li>
              <form action="{{ url('lone/amount') }}" method="post">
                {{ csrf_field() }}
                <input type="text" value="{{ $account->id }}" name="account_id" hidden>
                <input type="text" value="{{ \App\Classes\Helper::getNextAmountDate($account->id) }}" name="amount_date" hidden>
                <input type="text" value="{{ $account->saving_amount }}" name="saving_amount" hidden>
                <input type="text" value="{{ $account->lone_total_amount }}" name="lone_amount" hidden>
              <button class="btn btn-primary btn-block btn-flat" type="submit">জমাঃ {{ $account->lone_total_amount }} + {{ $account->saving_amount }} = {{ $account->lone_total_amount + $account->saving_amount }} </button>
              </form>
            </li>
          </ul>
        </div>
        <div class="progress xs">
          <div class="progress-bar progress-bar-green" style="width: {{ \App\Classes\Helper::lonePercentage($account->id) }}%"></div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</section>
<!-- /.content -->
@endcan
@endsection

