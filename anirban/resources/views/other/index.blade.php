@extends('layouts.app')

@section('title', 'অন‍্যান‍্য')
@section('content-title', 'অন‍্যান‍্য')
@section('content-subtitle', 'অন‍্যান‍্য জমা ও খরচ লিস্ট')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('other.index') }}">অন‍্যান‍্য জমা ও খরচ লিস্ট</a></li>
@endsection

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}>
@endsection

@section('content')
@can('other.view', Auth::user())
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header">
      {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>তারিখ</th>
            <th>বিস্তারিত</th>
            <th>খরচ</th>
            <th>জমা</th>
            @can('other.update')
            <th></th>
            @endcan
          </tr>
        </thead>
        <tbody>
          @foreach($others as $other)
          <tr>
            <td>{{ \App\Classes\Helper::getDate($other->other_date) }}</td>
            <td>{{ $other->other_description }}</td>
            <td>{{ $other->elevation_amount }}</td>
            <td>{{ $other->saving_amount }}</td>
            @can('other.update')
            <td>
              <a href="{{ route('other.edit', $other->id) }}" class="btn btn-info btn-xs"><span class="fa fa-edit"></span></a>

              @can('other.delete')
              <form id="delete-form-{{ $other->id }}" method="post" action="{{ route('other.destroy',$other->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
              <a class="btn btn-danger btn-xs" href="" onclick="
              if(confirm('Are you sure, You Want to delete this?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $other->id }}').submit();
                  }
                  else{
                    event.preventDefault();
                  }" ><span class="fa fa-trash"></span></a>
               @endcan   
            </td>
            @endcan
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>তারিখ</th>
            <th>বিস্তারিত</th>
            <th>খরচ</th>
            <th>জমা</th>
            @can('other.update')
            <th></th>
            @endcan
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/datatables.net/js/jquery.dataTables.min.js") }}></script>
<script src={{ asset("admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}></script>
<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>
@endsection