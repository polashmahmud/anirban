@extends('layouts.app')

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/select2/dist/css/select2.min.css") }}>
@endsection

@section('title', 'অন‍্যান‍্য')
@section('content-title', 'অন‍্যান‍্য')
@section('content-subtitle', 'অন‍্যান‍্য খরচ জমা পেজ')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li ><a href="{{ route('other.index') }}">অন‍্যান‍্য খরচ জমা লিস্ট</a></li>
<li class="active"><a href="{{ route('other.create') }}">অন‍্যান‍্য খরচ জমা তৈরি</a></li>
@endsection

@section('content')
@can('other.update', Auth::user())
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">অন‍্যান‍্য খরচ জমা সম্পাদন</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('other.update', $other->id) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="box-body">

            <div class="form-group">
              <label for="name">তারিখ</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" id="datepicker" name="other_date" data-date-format="yyyy/mm/dd" value="{{ $other->other_date }}">
              </div>
            </div>

            <div class="form-group">
              <label>বিস্তারিত</label>
              <textarea type="text" class="form-control" id="contact_number" placeholder="Description" name="other_description">{{ $other->other_description }}</textarea>
            </div>

            <div class="form-group">
              <label for="elevation_amount">খরচ</label>
              <input class="form-control" id="elevation_amount" placeholder="Cost Amount" type="number" name="elevation_amount" value="{{ $other->elevation_amount }}">
            </div>

            <div class="form-group">
              <label for="saving_amount">জমা</label>
              <input class="form-control" id="saving_amount" placeholder="Saving Amount" type="number" name="saving_amount" value="{{ $other->saving_amount }}">
            </div>
            
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">সম্পাদন করুন</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ব‍্যবহার বিধি</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            
          </div>
      </div>
    </div>
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/moment/min/moment.min.js") }}></script>
<script src={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}></script>
<!-- bootstrap datepicker -->
<script src={{ asset("admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}></script>
<script src={{ asset("admin/bower_components/select2/dist/js/select2.full.min.js") }}></script>

<script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
    $('.select2').select2()
  })
</script>
@endsection
