@extends('layouts.app')

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}>
@endsection

@section('title', 'প্রফাইল')
@section('content-title', 'প্রফাইল')
@section('content-subtitle', 'প্রফাইল সম্পাদন')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li ><a href="{{ route('member.index') }}">সদস‍্য লিস্ট</a></li>
<li class="active"><a href="#">প্রফাইল</a></li>
@endsection

@section('content')
@can('users.view', Auth::user())
<!-- Main content -->
<section class="content">

  <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              @if(empty($profile->photo))  
                <img class="profile-user-img img-responsive img-circle" src={{ asset("admin/dist/img/avatar04.png") }} alt="User profile picture">
              @else
                <img class="profile-user-img img-responsive img-circle" src="{{-- {{ Storage::url($profile->photo) }} --}} {{ asset($profile->photo) }}" alt="User profile picture" style="width: 100px; height: 100px;">
              @endif
              

              <h3 class="profile-username text-center">{{ $profile->user->name }}</h3>

              <p class="text-muted text-center">Email: {{ $profile->user->email }}</p>

              {{-- <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">13,287</a>
                </li>
              </ul> --}}

              {{-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> --}}
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">প্রফাইল</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form class="form-horizontal" @can('users.view', Auth::user()) action="{{ url('member/profile', $profile->id) }}" method="post" @endcan enctype="multipart/form-data">
             @can('users.create', Auth::user()) {{ csrf_field() }} @endcan
             
              <div class="box-body">

                <div class="form-group">
                  <label for="father_name" class="col-sm-3 control-label">বাবার নাম</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="father_name" placeholder="Father Name" name="father_name" value="{{ $profile->father_name }}">
                  </div>
                </div>

                <div class="form-group">
                  <label for="mother_name" class="col-sm-3 control-label">মার নাম</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="mother_name" placeholder="Mother Name" name="mother_name" value="{{ $profile->mother_name }}">
                  </div>
                </div>

                <div class="form-group">
                  <label for="contact_number" class="col-sm-3 control-label">যোগাযোগ নম্বর</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="contact_number" placeholder="Contact Number" name="contact_number" value="{{ $profile->contact_number }}">
                  </div>
                </div>

                <div class="form-group">
                  <label for="alternate_contact_number" class="col-sm-3 control-label">যোগাযোগ নম্বর</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="alternate_contact_number" placeholder="Alternate Contact Number" name="alternate_contact_number" value="{{ $profile->alternate_contact_number }}">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">জন্মদিন</label>
                  <div class="col-sm-9">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="datepicker" name="date_of_birth" value="{{ $profile->date_of_birth }}" data-date-format="yyyy/mm/dd">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">যোগদানের তারিখ</label>
                  <div class="col-sm-9">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="datepicker1" name="date_of_joining" value="{{ $profile->date_of_joining }}" data-date-format="yyyy/mm/dd">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="contact_number" class="col-sm-3 control-label">বর্তমান ঠিকানা</label>
                  <div class="col-sm-9">
                    <textarea type="text" class="form-control" id="contact_number" placeholder="Present Address" name="present_address">{{ $profile->present_address }}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="contact_number" class="col-sm-3 control-label">স্থায়ী ঠিকানা</label>
                  <div class="col-sm-9">
                    <textarea type="text" class="form-control" id="contact_number" placeholder="Present Address" name="permanent_address">{{ $profile->permanent_address }}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="contact_number" class="col-sm-3 control-label">ফেসবুক আইডি</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="contact_number" placeholder="facebook Id" name="facebook_link" value="{{ $profile->facebook_link }}">
                  </div>
                </div>

                <div class="form-group">
                  <label for="contact_number" class="col-sm-3 control-label">ছবি</label>
                  <div class="col-sm-9">
                    <input type="file" id="exampleInputFile" name="photo">
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              @can('users.view', Auth::user())
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">প্রফাইল আপডেট</button>
              </div>
              @endcan
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/moment/min/moment.min.js") }}></script>
<script src={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}></script>
<!-- bootstrap datepicker -->
<script src={{ asset("admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}></script>

<script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
    $('#datepicker1').datepicker({
      autoclose: true
    })
  })
</script>
@endsection
