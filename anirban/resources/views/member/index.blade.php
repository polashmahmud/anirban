@extends('layouts.app')

@section('title', 'Member page')
@section('content-title', 'সদস‍্য সমূহ')
@section('content-subtitle', 'সমিতির সকল সদস‍্য সমূহ')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('member.index') }}">সদস‍্য সমূহ</a></li>
@endsection

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}>
@endsection

@section('content')
@can('users.view')
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header">
      {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>সদস‍্য নাম</th>
            <th>ইমেল</th>
            <th>সদস‍্য কোড</th>
            <th>মোবাইল</th>
            <th>যোগদানের তারিখ</th>
            <th>স্টাটার্স</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->profile->anirban_code }}</td>
            <td>{{ $user->profile->contact_number }}</td>
            <td>{{ $user->profile->date_of_joining }}</td>
            <td><a href="@can('users.create') {{ url('member/user', $user->id) }} @endcan">
              @if($user->active == 1) 
                <span class="fa fa-check"> একটিভ</span>
              @elseif($user->active == 0)
                <span class="fa fa-ban" style="color: red"> ডিএকটিভ</span>
              @endif
            </a></td>
            <td>
              <a href="{{ route('member.show', $user->id) }}" class="btn btn-success btn-xs"><span class="fa fa-user"></span></a>
              @can('users.update')
              <a href="{{ route('member.edit', $user->id) }}" class="btn btn-info btn-xs"><span class="fa fa-edit"></span></a>
              @endcan
              @can('users.delete')
              <form id="delete-form-{{ $user->id }}" method="post" action="{{ route('member.destroy',$user->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
              <a class="btn btn-danger btn-xs" href="" onclick="
              if(confirm('Are you sure, You Want to delete this?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $user->id }}').submit();
                  }
                  else{
                    event.preventDefault();
                  }" ><span class="fa fa-trash"></span></a>
             @endcan     
            </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>সদস‍্য নাম</th>
            <th>ইমেল</th>
            <th>সদস‍্য কোড</th>
            <th>মোবাইল</th>
            <th>যোগদানের তারিখ</th>
            <th>স্টাটার্স</th>
            <th></th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/datatables.net/js/jquery.dataTables.min.js") }}></script>
<script src={{ asset("admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}></script>
<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>
@endsection