@extends('layouts.app')

@section('title', 'Member Create')
@section('content-title', 'সদস‍্য যুক্ত করুন')
@section('content-subtitle', 'সমিতিতে নতুন সদস‍্য যুক্ত করুন')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li ><a href="{{ route('member.index') }}">সদস‍্য সমূহ</a></li>
<li class="active"><a href="{{ route('member.create') }}">নতুন সদস‍্য যুক্ত করুন</a></li>
@endsection

@section('content')
@can('users.create')
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">নতুন সদস‍্য যুক্ত করুন</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('member.store') }}" method="post">
          {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label for="name">সদস‍্য নাম</label>
              <input class="form-control" id="name" placeholder="Full Name" type="name" name="name" value="{{ old('name') }}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">ইমেল এড্রেস</label>
              <input class="form-control" id="exampleInputEmail1" placeholder="Enter email" type="email" name="email" value="{{ old('email') }}">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">পাসওয়ার্ড</label>
              <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password" name="password">
            </div>
            <div class="form-group">
              <label for="password_confirmation">পুণরায় পাসওয়ার্ড</label>
              <input class="form-control" id="password_confirmation" placeholder="Retype Password" type="password" name="password_confirmation">
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">নতুন সদস‍্য যুক্ত করুন</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ব‍্যবহার বিধি</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            
          </div>
      </div>
    </div>
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endcan
@endsection
