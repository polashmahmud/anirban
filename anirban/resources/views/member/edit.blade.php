@extends('layouts.app')

@section('title', 'Member Edit')
@section('content-title', 'সদস‍্য আপডেট পেজ')
@section('content-subtitle', 'সমিতির সদস‍্য ইনফরমেশন আপডেট করুন')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li ><a href="{{ route('member.index') }}">সদস‍্য সমূহ</a></li>
<li class="active"><a href="#">সদস‍্য আপডেট</a></li>
@endsection

@section('content')
@can('users.update')
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">সদস‍্য আপডেট</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('member.update', $user->id) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="box-body">
            <div class="form-group">
              <label for="name">নাম</label>
              <input class="form-control" id="name" placeholder="Full Name" type="name" name="name" value="{{ $user->name }}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">ইমেল এড্রেস</label>
              <input class="form-control" id="exampleInputEmail1" placeholder="Enter email" type="email" name="email" value="{{ $user->email }}" disabled>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">পাসওয়ার্ড</label>
              <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password" name="password">
            </div>
            <div class="form-group">
              <label for="password_confirmation">পুনরায় পাসওয়ার্ড</label>
              <input class="form-control" id="password_confirmation" placeholder="Retype Password" type="password" name="password_confirmation">
            </div>
            <div class="row">
              @foreach($roles as $role)
              <div class="col-md-3">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="{{ $role->id }}" name="role[]" 
                    @foreach($user->roles as $user_role)
                      @if($user_role->id == $role->id) checked @endif
                    @endforeach
                    > {{ $role->name }}
                  </label>
                </div>
              </div>
              @endforeach
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">আপডেট করুন</button>
            <a href="{{ route('home') }}" class="btn btn-default pull-right">Back</a>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ব‍্যবহার বিধি</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            
          </div>
      </div>
    </div>
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endcan
@endsection
