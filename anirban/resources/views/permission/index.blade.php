@extends('layouts.app')

@section('title', 'Permission')
@section('content-title', 'Permission')
@section('content-subtitle', 'All Permission')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('role.index') }}">Permission</a></li>
@endsection


@section('content')
@can('role.view', Auth::user())
<section class="content">
<!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      @can('role.create')
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Permission Create From</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('permission.store') }}" method="post">
          {{ csrf_field() }}
          <div class="box-body">
            <div class="form-group">
              <label>PermissionName</label>
              <input type="text" class="form-control"  name="name" placeholder="Permission Name">
            </div>
            <div class="form-group">
              <label>Permission For</label>
              <select class="form-control" name="for">
                <option value="1">ডেসবোর্ড</option>
                <option value="2">একাউন্টস সমূহ</option>
                <option value="3">সদস‍্য সমূহ</option>
                <option value="4">সঞ্চয় ও ঋণ সমূহ</option>
                <option value="5">অন‍্যান‍্য</option>
                <option value="6">টাকা সমূহ</option>
                <option value="7">Tasks</option>
                <option value="8">User Role</option>
                <option value="9">কালেকশন সমূহ</option>
              </select>
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Add New permission</button>
          </div>
        </form>
      </div>
      @endcan
    </div>
    <div class="col-md-6">
      @if($permissions->isNotEmpty())
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">All Permission</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-striped">
            <tbody><tr>
              <th style="width: 10px">#</th>
              <th>Permission Name</th>
              <th>Permission For</th>
              <th style="width: 100px"></th>
            </tr>
            @foreach($permissions as $permission)
            <tr>
              <td>{{ $permission->id }}</td>
              <td>{{ $permission->name }}</td>
              <td>
                @if($permission->for == 1) ডেসবোর্ড
                @elseif($permission->for == 2) একাউন্টস সমূহ
                @elseif($permission->for == 3) সদস‍্য সমূহ
                @elseif($permission->for == 4) সঞ্চয় ও ঋণ সমূহ
                @elseif($permission->for == 5) অন‍্যান‍্য
                @elseif($permission->for == 6) টাকা সমূহ
                @elseif($permission->for == 7) Tasks
                @elseif($permission->for == 8) User Role
                @elseif($permission->for == 9) কালেকশন সমূহ
                @else
                Muri Khan
                @endif
              </td>
              <td>
                @can('role.update', Auth::user())
                <a href="{{ route('permission.edit', $permission->id) }}" class="btn btn-info btn-xs"><span class="fa fa-edit"></span></a>
                @endcan
                @can('role.delete', Auth::user())
              <form id="delete-form-{{ $permission->id }}" method="post" action="{{ route('permission.destroy',$permission->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
              <a class="btn btn-danger btn-xs" href="" onclick="
              if(confirm('Are you sure, You Want to delete this?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $permission->id }}').submit();
                  }
                  else{
                    event.preventDefault();
                  }" ><span class="fa fa-trash"></span></a>
                  @endcan
              </td>
            </tr>
            @endforeach
          </tbody></table>
        </div>
        <!-- /.box-body -->
      </div>
      @endif
    </div>
  </div>
  <!-- /.box -->
</section>
@endcan
@endsection

