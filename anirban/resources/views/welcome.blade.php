<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>অনির্বাণ সঞ্চয় ও ঋণদান সমিতি</title>

    <!-- Bootstrap core CSS -->
    <link href="user/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="user/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Galada|Hind+Siliguri:300,400,500,600,700" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="user/css/landing-page.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">অনির্বাণ সঞ্চয় ও ঋণদান সমিতি</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">

            @if(Auth::guest())
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">প্রবেশ</a>
            </li>  
            @endif 
            @auth
                @foreach(Auth::user()->roles as $role)
                    @if($role->name == 'user')
                        <li><a href="{{ route('logout') }}" class="btn btn-default btn-flat"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          প্রস্থান
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                        </form></li>
                        <li><a href="{{ route('dashboard') }}" class="nav-link" >ডেসবোর্ড</a></li>
                     @else
                        <li><a href="{{ route('logout') }}" class="btn btn-default btn-flat"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          প্রস্থান
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                        </form></li>
                        <li><a href="{{ url('/dashboard') }}" class="nav-link" >এডমিন প‍্যানেল</a></li>
                     @endif
                @endforeach
            @endauth
            
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="intro-header">
      <div class="container">
        <div class="intro-message">
          <h1>অনির্বাণ সঞ্চয় ও ঋণদান সমিতি</h1>
          <h3>সম্পূর্ণ সুদ মুক্ত একটি প্রতিষ্ঠান</h3>
          <hr class="intro-divider">
          <ul class="list-inline intro-social-buttons">
            <li class="list-inline-item">
              <a href="#" class="btn btn-secondary btn-lg">
                <i class="fa fa-user fa-fw"></i>
                <span class="network-name">{{ \App\User::count('id') }} জন সদস‍্য</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </header>

    <!-- Page Content -->
    <section class="content-section-a">

      <div class="container">
        <div class="row">
          <div class="col-lg-5 ml-auto">
            <hr class="section-heading-spacer">
            <div class="clearfix"></div>
            <h2 class="section-heading">পরিচিতি</h2>
            <p class="lead">অনির্বাণ সঞ্চয় ও ঋণদান সমিতি একটি অলাভ জনক ও ক্ষুদ্র প্রতিষ্ঠান। এটি একটি পরিবারের মতো এবং এটি বিশ্বাস করে ঐক‍্যবদ্ধ প্ররিশ্রমই উন্নতি আনে। সঞ্চয়ের মাধ‍্যমে সদস‍্যগনকে স্বাবলম্বী হিসাবে গড়ে তুলে একটি সমৃদ্ধশালী সমাজ প্রতিষ্ঠা করা আমাদের লক্ষ‍্য। আমরা ক্ষুদ্র সঞ্চয় সুবিধা প্রদান করে অর্থনৈতিক উন্নতি সাধন এবং উন্নয়ন মূলক কর্মকান্ডে অংশগ্রহন করি।সদস‍্যগনকে জমি এবং বাড়ি নির্মানে উৎসাহ ও সহযোগিতা প্রদান করি। </p>
          </div>
          <div class="col-lg-5 mr-auto">
            <img class="img-fluid" src="user/img/family.png" alt="">
          </div>
        </div>

      </div>
      <!-- /.container -->
    </section>

    <section class="content-section-b">

      <div class="container">

        <div class="row">
          <div class="col-lg-5 mr-auto order-lg-2">
            <hr class="section-heading-spacer">
            <div class="clearfix"></div>
            <h2 class="section-heading">আমরা কাদের ঋণ দিয়ে থাকি?</h2>
            <p class="lead">যারা কাজ করতে আগ্রহী, কর্মঠ কিন্তু টাকার অভাবে প্রতিষ্ঠিত হতে পারছে না। আমরা তাদের ঋণ প্রদান করে স্বাবলম্ভী হবার সুযোগ সৃস্টি করে থাকি। যাদের দোকান আছে কিন্তু টাকার অভাবে সুন্দর ভাবে পরিচালনা করতে পারছে না আমরা তাদের ঋণ দিয়ে থাকি। আমরা সম্পূর্ন সুদ বিহীন ভাবে ঋণ প্রদান করে থাকি।</p>
          </div>
          <div class="col-lg-5 ml-auto order-lg-1">
            <img class="img-fluid" src="user/img/shop.png" alt="">
          </div>
        </div>

      </div>
      <!-- /.container -->

    </section>
    <!-- /.content-section-b -->

    <section class="content-section-a">

      <div class="container">

        <div class="row">
          <div class="col-lg-5 ml-auto">
            <hr class="section-heading-spacer">
            <div class="clearfix"></div>
            <h2 class="section-heading">কোন প্রকার সুদ / লাভ না নিয়ে সমিতি চলছে কি করে?</h2>
            <p class="lead">আমাদের এখানে যারা সদস‍্য হন তাদের ভর্তি ফরম বাবদ ১০০ টাকা এবং পরবর্তিতে প্রতিবার ঋণ গ্রহন করবার সময় ঋণ ফরম বাবদ ৫০ টাকা চার্জ নেওয়া হয়, এছাড়াও কেউ ঋণ নিয়ে সম্পূর্ন পরিশোধ করবার পর যদি মনে করে এই টাকা দিয়ে তার ব‍্যবসায় উন্নতি হয়েছে তাহলে সে খুশি হয়ে আমাদের যা প্রদান করে তাই দিয়ে আমাদের এই প্রতিষ্ঠানটি চলছে।</p>
          </div>
          <div class="col-lg-5 mr-auto ">
            <img class="img-fluid" src="user/img/happy.png" alt="">
          </div>
        </div>

      </div>
      <!-- /.container -->

    </section>
    <!-- /.content-section-a -->

    <section class="content-section-b">

      <div class="container">
        <h1 class="text-center">আমাদের সুবিধা সমূহ</h1>
          <hr>

        <div class="row">
          <div class="col-lg-4 my-auto text-center">
            <h3>ক্ষুদ্র সঞ্চয়</h3>
            <p>আমরা ১০ টাকা থেকে শুরু করে ৫০ টাকা পর্যন্ত দৈনিক, ১০০ টাকা থেকে ৫০০ টাকা পর্যন্ত সপ্তাহিত অথবা ১,০০০ থেকে ৫,০০০ টাকা পর্যন্ত মাসিক সঞ্চয় সুবিধা প্রদান করে থাকি।</p>
          </div>
          <div class="col-lg-4 my-auto text-center">
            <h3>ক্ষুদ্র ঋণ</h3>
            <p>আমরা দৈনিক, সাপ্তাহিক এবং মাসিক কিস্তিতে ঋণ প্রদান করে থাকি। ঋণের পরিমান ৫,০০০ টাকা থেকে সর্বচ্চ ৫০,০০০ টাকা পর্যন্ত।</p>
          </div>
          <div class="col-lg-4 my-auto text-center">
            <h3>সামাজিক উন্নয়ন</h3>
            <p>আমরা সকল সদস‍্য হতে ২০ টাকা করে প্রতি মাসে জমা রাখি যা দিয়ে সামাজিক উন্নয়নমূলক কর্মকান্ডে অংশগ্রহণ করা হয়।</p>
          </div>
        </div>
        <hr>
        <h6 class="text-center">আমরা যেহেতু ঋণের পরিমানের উপর কোন প্রকার সুদ গ্রহণ করে থাকি না তাই আমরা আমাদের প্রতিষ্ঠানে যারা সঞ্চয় প্রদান করে তাদেরকে কোন প্রকার নির্দিষ্ঠ লভ‍্যাংশ প্রদান করে থাকি না। সমিতিতে যদি লাভ হয় তাহলে লাভের অংশ বন্টন করে দেওয়া হয়।</h6>

      </div>
      <!-- /.container -->

    </section>
    <!-- /.content-section-b -->

    <aside class="banner">

      <div class="container">

        <div class="row">
          <div class="col-lg-6 my-auto">
            <h2>আমাদের সাথে যোগাযোগ করার মাধ‍্যমঃ</h2>
          </div>
          <div class="col-lg-6 my-auto">
            <ul class="list-inline banner-social-buttons">
              <li class="list-inline-item">
                <a href="#" class="btn btn-secondary btn-lg">
                  <i class="fa fa-phone fa-fw"></i>
                  <span class="network-name">01678-126086</span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.facebook.com/anirban.somity/" class="btn btn-secondary btn-lg" target="_blank">
                  <i class="fa fa-facebook fa-fw"></i>
                  <span class="network-name">Facebook</span>
                </a>
              </li>
            </ul>
          </div>
        </div>

      </div>
      <!-- /.container -->

    </aside>
    <!-- /.banner -->

    <!-- Footer -->
    <footer>
      <div class="container">
       <!--  <ul class="list-inline">
          <li class="list-inline-item">
            <a href="#">Home</a>
          </li>
          <li class="footer-menu-divider list-inline-item">&sdot;</li>
          <li class="list-inline-item">
            <a href="#about">About</a>
          </li>
          <li class="footer-menu-divider list-inline-item">&sdot;</li>
          <li class="list-inline-item">
            <a href="#services">Services</a>
          </li>
          <li class="footer-menu-divider list-inline-item">&sdot;</li>
          <li class="list-inline-item">
            <a href="#contact">Contact</a>
          </li>
        </ul> -->
        <p class="copyright text-muted small">Copyright &copy; অনির্বাণ সঞ্চয় ও ঋণদান সমিতি ২০১৭</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="user/vendor/jquery/jquery.min.js"></script>
    <script src="user/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script>
    $("th, small, kbd, code, span").each(function(){
        $(this).text(en2bn($(this).text()));
    });
    function en2bn(input){
        var en = ["1","2","3","4","5","6","7","8","9","0",'January','th of ','Saturday','PM','AM','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','nd of','February','March','April','May','June','July','August','September','October','November','December','rd of'];
        var bn = ["১","২","৩","৪","৫","৬","৭","৮","৯","০",'জানুয়ারি', ', ','শনিবার',' ',' ','রবিবার','সোমবার','মঙ্গলবার','বুধবার','বৃহস্পতিবার','শুক্রুবার',' ','ফেব্রুয়ারী','মার্চ','এপ্রিল','মে','জুন','জুলাই','আগস্ট','সেপ্টেম্বর','অক্টবর','নভেম্বর','ডিসেম্বর',' '];
        input = input.toString();
        // use array length
        for( var i = 0; i < en.length ; i++)
        {
//                input = input.replace( en[i] , bn[i] );
            var re = new RegExp(en[i] ,'g');
            input = input.replace(re,  bn[i]);
        }
        return input;
    }
    $( document ).ready(function() {
        /*var html = $('#trns').html();
         html = en2bn(html);
         $('#trns').html(html);*/
    });
</script>

  </body>

</html>
