@extends('layouts.app')

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/select2/dist/css/select2.min.css") }}>
@endsection

@section('title', 'একাউন্ট ক্রিয়েট পেজ')
@section('content-title', 'একাউন্ট ক্রিয়েট পেজ')
@section('content-subtitle', 'সদস‍্যদের ঋন ও সঞ্চয় ক্রিয়েট ফরম')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li ><a href="{{ route('account.index') }}">একাউন্ট লিস্ট</a></li>
<li class="active"><a href="{{ route('account.create') }}">একাউন্ট ক্রিয়েট</a></li>
@endsection

@section('content')
@can('lone.create', Auth::user())
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">সঞ্চয় ও ঋণ ফরম</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('account.store') }}" method="post">
          {{ csrf_field() }}
          <div class="box-body">

            <div class="form-group">
              <label>সদস‍্য নাম</label>
              <select class="form-control select2" style="width: 100%;" name="user_id">
                @foreach($users as $user)
                <option value="{{ $user->id }}">{{ $user->name }} - {{ $user->profile->contact_number }} ({{ $user->profile->anirban_code }})</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="name">তারিখ</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" id="datepicker" name="date_of_account" data-date-format="yyyy/mm/dd">
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <div class="radio">
                    <label>
                      <input type="radio" name="account_type" id="account_type1" value="0" checked=""> ঋণ একাউন্ট
                    </label>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="radio">
                    <label>
                      <input type="radio" name="account_type" id="account_type2" value="1"> সঞ্চয় একাউন্ট
                    </label>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="radio">
                    <label>
                      <input type="radio" name="account_type" id="account_type2" value="2"> ফিক্সড একাউন্ট
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="day">দিন</label>
                  <input class="form-control" id="day" type="number" name="day" value="{{ old('day') }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="week">সপ্তাহ</label>
                  <input class="form-control" id="week" type="number" name="week" value="{{ old('week') }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="month">মাস</label>
                  <input class="form-control" id="month" type="number" name="month" value="{{ old('month') }}">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="lone_amount">ঋণ</label>
                  <input class="form-control" id="lone_amount" type="number" name="lone_amount" value="{{ old('lone_amount') }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="lone_total_amount">প্রতিদিন ঋণ জমা</label>
                  <input class="form-control" id="lone_total_amount" type="number" name="lone_total_amount" value="{{ old('lone_total_amount') }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="saving_amount">সঞ্চয়</label>
                  <input class="form-control" id="saving_amount" type="number" name="saving_amount" value="{{ old('saving_amount') }}">
                </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">নতুন একাউন্ট তৈরি করুন</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ব‍্যবহার বিধি</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            
          </div>
      </div>
    </div>
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/moment/min/moment.min.js") }}></script>
<script src={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}></script>
<!-- bootstrap datepicker -->
<script src={{ asset("admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}></script>
<script src={{ asset("admin/bower_components/select2/dist/js/select2.full.min.js") }}></script>

<script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
    $('.select2').select2()
  })
</script>
@endsection
