@extends('layouts.app')

@section('title', 'একাউন্ট পেজ')
@section('content-title', 'একাউন্ট পেজ')
@section('content-subtitle', 'একাউন্ট লিস্ট')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('account.index') }}">একাউন্ট</a></li>
@endsection

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}>
@endsection

@section('content')
@can('lone.view', Auth::user())
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header">
      {{-- <h3 class="box-title">Data Table With Full Features</h3> --}}
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>একাউন্ট নং</th>
            <th>নাম</th>
            <th>তারিখ</th>
            <th>দিন</th>
            <th>সপ্তাহ</th>
            <th>মাস</th>
            <th>ঋণ</th>
            <th>ঋণ জমা</th>
            <th>সঞ্চয়</th>
            <th>একাউন্ট টাইপ</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($accounts as $account)
          <tr>
            <td>AAN-{{ $account->id }}</td>
            <td>{{ $account->user->name }}</td>
            <td>{{ \App\Classes\Helper::getDate($account->date_of_account) }}</td>
            <td>{{ $account->day }}</td>
            <td>{{ $account->week }}</td>
            <td>{{ $account->month }}</td>
            <td>{{ $account->lone_amount }}</td>
            <td>{{ $account->lone_total_amount }}</td>
            <td>{{ $account->saving_amount }}</td>
            <td>
              @if($account->account_type == 0)
              <span class="badge bg-red">Lone</span>
              @elseif($account->account_type == 1)
              <span class="badge bg-blue">Saving</span>
              @else
              <span class="badge bg-green">Fixed</span>
              @endif
            </td>
            <td>
              @can('lone.create', Auth::user())
              @if($account->status == 0)
              <a class="btn btn-default btn-xs" href="{{ url('account/lone', $account->id) }}"><span class="fa fa-spinner"></span></a>
              @else
              <a class="btn btn-xs btn-success" href="{{ url('account/lone', $account->id) }}"><span class="fa fa-check"></span></a>
              @endif
              @endcan
              
              @can('lone.update', Auth::user())
              <a href="{{ route('account.edit', $account->id) }}" class="btn btn-info btn-xs"><span class="fa fa-edit"></span></a>
              @endcan

              @can('lone.delete', Auth::user())
              <form id="delete-form-{{ $account->id }}" method="post" action="{{ route('account.destroy',$account->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
              <a class="btn btn-danger btn-xs" href="" onclick="
              if(confirm('Are you sure, You Want to delete this?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $account->id }}').submit();
                  }
                  else{
                    event.preventDefault();
                  }" ><span class="fa fa-trash"></span></a>
               @endcan
            </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>একাউন্ট নং</th>
            <th>নাম</th>
            <th>তারিখ</th>
            <th>দিন</th>
            <th>সপ্তাহ</th>
            <th>মাস</th>
            <th>ঋণ</th>
            <th>ঋণ জমা</th>
            <th>সঞ্চয়</th>
            <th>একাউন্ট টাইপ</th>
            <th></th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/datatables.net/js/jquery.dataTables.min.js") }}></script>
<script src={{ asset("admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}></script>
<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>
@endsection