@extends('layouts.app')

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/select2/dist/css/select2.min.css") }}>
@endsection

@section('title', 'একাউন্ট এডিট পেজ')
@section('content-title', 'একাউন্ট এডিট পেজ')
@section('content-subtitle', 'একাউন্ট এডিট পেজ')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('account.index') }}">একাউন্ট লিস্ট</a></li>
@endsection

@section('content')
@can('lone.update', Auth::user())
<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">একাউন্ট সপ্তাদান পেজ</h3>
        </div>
        @include('layouts.message')
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{ route('account.update', $account->id) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <div class="box-body">
              
            <div class="form-group">
              <label for="user_id">সদস‍্য নং</label>
              <input class="form-control" disabled id="user_id" type="text" value="{{ $account->user->name }} - {{ $account->user->profile->contact_number }} ( {{ $account->user->profile->anirban_code }} )">
            </div>
              
            <div class="form-group">
              <label for="name">তারিখ</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right" id="datepicker" name="date_of_account" data-date-format="yyyy/mm/dd" value="{{ $account->date_of_account }}">
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <div class="radio">
                    <label>
                      <input type="radio" name="account_type" id="account_type1" value="0" @if($account->account_type == 0) checked="checked" @endif > ঋণ একাউন্ট
                    </label>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="radio">
                    <label>
                      <input type="radio" name="account_type" id="account_type2" value="1" @if($account->account_type == 1) checked="checked" @endif > সঞ্চয় একাউন্ট
                    </label>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="radio">
                    <label>
                      <input type="radio" name="account_type" id="account_type3" value="2" @if($account->account_type == 2) checked="checked" @endif > ফিক্সড একাউন্ট
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="day">দিন</label>
                  <input class="form-control" id="day" type="number" name="day" value="{{ $account->day }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="week">সপ্তাহ</label>
                  <input class="form-control" id="week" type="number" name="week" value="{{ $account->week }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="month">মাস</label>
                  <input class="form-control" id="month" type="number" name="month" value="{{ $account->month }}">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="lone_amount">ঋণ</label>
                  <input class="form-control" id="lone_amount" placeholder="Lone Amount" type="number" name="lone_amount" value="{{ $account->lone_amount }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="lone_total_amount">প্রতিদিন ঋণ জমা</label>
                  <input class="form-control" id="lone_total_amount" placeholder="Total Lone Amount" type="number" name="lone_total_amount" value="{{ $account->lone_total_amount }}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="saving_amount">সঞ্চয়</label>
                  <input class="form-control" id="saving_amount" placeholder="Saving Amount" type="number" name="saving_amount" value="{{ $account->saving_amount }}">
                </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">সম্পাদন একাউন্ট</button>
            <a href="{{ route('account.show', $account->id) }}" class="btn btn-default pull-right">Back</a>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ব‍্যবহার বিধি</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
            
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            
          </div>
      </div>
    </div>
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/moment/min/moment.min.js") }}></script>
<script src={{ asset("admin/bower_components/bootstrap-daterangepicker/daterangepicker.js") }}></script>
<!-- bootstrap datepicker -->
<script src={{ asset("admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}></script>
<script src={{ asset("admin/bower_components/select2/dist/js/select2.full.min.js") }}></script>

<script>
  $(function () {

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
    $('.select2').select2()
  })
</script>
@endsection
