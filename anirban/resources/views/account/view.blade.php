@extends('layouts.app')

@section('title', 'একাউন্ট পেজ')
@section('content-title', 'একাউন্ট পেজ')
@section('content-subtitle', 'একাউন্ট লিস্ট')
@section('link-list')
<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
<li class="active"><a href="{{ route('account.index') }}">একাউন্ট</a></li>
@endsection

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}>
@endsection

@section('content')
@can('lone.view', Auth::user())
<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-4">
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header white">
        <h3 class="widget-user-username">{{ $account->user->name }}</h3>
        <h5 class="widget-user-desc">সদস‍্যঃ @can('lone.create', Auth::user())<a href="{{ route('member.edit', $account->user->id) }}">@endcan AnirBan-{{ $account->user->id }}@can('lone.create', Auth::user())</a>@endcan</h5>
        <h6 class="widget-user-desc">ঋণঃ @can('lone.create', Auth::user())<a href="{{ route('account.edit', $account->id) }}">@endcan AAN-{{ $account->id }}@can('lone.create', Auth::user())</a>@endcan</h6>
      </div>
      <div class="widget-user-image">
        @if(isset($account->user->profile->photo))
          <img src={{ asset($account->user->profile->photo) }} class="img-circle" alt="User Image">
        @else
          <img class="img-circle" src={{ asset("admin/dist/img/user1-128x128.jpg") }} alt="User Avatar">
        @endif
        
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-sm-4 border-right">
            <div class="description-block">
              <h5 class="description-header">@if($account->month >= 1) {{ $account->month }} @elseif($account->week >= 1) {{ $account->week }} @elseif($account->day >= 1) {{ $account->day }} @else কোন ক‍্যাটাগরি সিলেট করা হয় নি @endif</h5>
              <span class="description-text">@if($account->month >= 1) মাসিক সঞ্চয় @elseif($account->week >= 1) সপ্তাহিত সঞ্চয় @elseif($account->day >= 1) দৈনিক সঞ্চয় @else কোন ক‍্যাটাগরি সিলেট করা হয় নি @endif</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-4 border-right">
            <div class="description-block">
              <h5 class="description-header">{{ \App\Classes\Helper::getNumber($account->saving_amount) }}</h5>
              <span class="description-text">সঞ্চয়</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-4">
            <div class="description-block">
              <h5 class="description-header">{{ \App\Classes\Helper::getNumber($account->lone_amount) }}</h5>
              <span class="description-text">ঋণ</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </div>
    <a href="{{ url('invoic/account-info', $account->id) }}" class="btn btn-default btn-block">বিস্তারিত প্রিন্ট</a>
    <a href="{{ url('invoic/account-form', $account->id) }}" class="btn btn-default btn-block">একাউন্ট ফরম</a>
  </div>
  <div class="col-md-8">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">জমা হিসাব</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tbody><tr>
            <th>#</th>
            <th>কিস্তি তারিখ</th>
            <th>জমা তারিখ</th>
            <th>সঞ্চয়</th>
            <th>ঋণ জমা</th>
            <th>উত্তোলন</th>
            <th>বোনাস</th>
            @can('lone.create', Auth::user())
            <th></th>
            @endcan
          </tr>
          @foreach($amounts as $data)
          <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ \App\Classes\Helper::getDate($data->amount_date) }}</td>
            <td>{{ \App\Classes\Helper::getDate($data->created_at) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->saving_amount) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->lone_amount) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->elevation_amount) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->bonus_amount) }}</td>
            @can('lone.create', Auth::user())
            <td>
              <a href="{{ route('amount.edit', $data->id) }}" class="btn btn-info btn-xs"><span class="fa fa-edit"></span></a>
              @can('lone.delete', Auth::user())
              <form id="delete-form-{{ $data->id }}" method="post" action="{{ route('amount.destroy',$data->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
              <a class="btn btn-danger btn-xs" href="" onclick="
              if(confirm('Are you sure, You Want to delete this?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $data->id }}').submit();
                  }
                  else{
                    event.preventDefault();
                  }" ><span class="fa fa-trash"></span></a>
              @endcan
            </td>
            @endcan
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th></th>
            <th></th>
            <th></th>
            <th>{{  $account->amount->sum('saving_amount')  }}</th>
            <th>{{  $account->amount->sum('lone_amount')  }}</th>
            <th>{{  $account->amount->sum('elevation_amount')  }}</th>
            <th>{{  $account->amount->sum('bonus_amount')  }}</th>
            <th></th>
          </tr>
        </tfoot>
      </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer clearfix">
        <div class="pull-right">
          {{ $amounts->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!-- /.content -->
@endcan
@endsection



