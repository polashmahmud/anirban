<!DOCTYPE html>
<html>
<title>AnirBan</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href={{ asset("mobile/css/w3.css") }}>
<link rel="stylesheet" href={{ asset("mobile/css/w3-theme-blue-grey.css") }}>

<link rel="stylesheet" href={{ asset("mobile/css/style.css") }}>
<link href="https://fonts.googleapis.com/css?family=Galada|Hind+Siliguri:400,700" rel="stylesheet">
<body>

<header class="w3-container w3-card-4 w3-theme">
  <h1><a href="{{ url('mobile/dashboard') }}">অনিরর্বাণ সমিতি</a></h1>
</header>

<div class="w3-panel">
	@yield('content')
</div>
</body>
</html> 
