@extends('mobile.layouts.app')

@section('content')
  <div class="w3-panel">
  	@can('accounts.view')
  	<h5>ক‍্যাশে টাকা আছেঃ <span class="w3-right">TK: {{ \App\Classes\Helper::getNumber($balance_amount) }}</span></h5>
  	<ul class="w3-ul w3-border">
  		@foreach($user_cash_balance as $row)
  		<li>
  			@if(Auth::id() == $row->user_id)
  			<a href="{{ url('mobile/payment', $row->user_id) }}">{{ \App\classes\Helper::getName($row->user_id) }}</a>
  			@else
  			{{ \App\classes\Helper::getName($row->user_id) }}
  			@endif 
  		<span class="w3-right">
  			@if($row->user_id == 1)
				TK: {{ \App\classes\Helper::getNumber($balance_amount - ($main_account_balance->amount_saving_amount + $main_account_balance->amount_lone_amount)) }}
  			@else
  			TK: {{ \App\classes\Helper::getNumber($row->amount_saving_amount + $row->amount_lone_amount) }}
  			@endif
  		</span>	
		@if($row->user_id != 1)
	        @can('amount.delete', Auth::user())
		        <span class="w3-right">
		          <form action="{{ url('amount/transfer') }}" method="post">
		          {{ csrf_field() }}  
		          <input type="text" value="{{ $row->user_id }}" name="user_id" hidden>
		          <button type="submit" class="w3-button" style="margin-top: -3px;margin-right: 15px;display:  inline-block;border:  none;padding: 0;" onclick="return confirm('are you sure?')"><img src="{{ asset("mobile/img/payment.png") }}" alt="" style="width: 30px;"></button>
		          </form>
		        </span>
	        @endcan
	    @endif
  		
  		</li>
  		@endforeach
  	</ul>
  	<hr>
  	@endcan
	<h5>আমার একাউন্ট সমূহঃ</h5>
	<ul class="w3-ul w3-border">
	@forelse ($accounts as $row)
	  <li>AAN-{{ $row->id }} <a href="{{ url('mobile/show', $row->id) }}" class="w3-right">বিস্তারিত</a></li>
	@empty
	    <p>No Accounts</p>
	@endforelse  
	</ul>
	@can('accounts.view')
	<h5>আরো দেখুনঃ</h5>
	<ul class="w3-ul w3-border">
		<li>ঋণ একাউন্ট <a href="{{ url('mobile/lone') }}" class="w3-right">বিস্তারিত</a></li>
		<li>সঞ্চয় একাউন্ট <a href="{{ url('mobile/saving') }}" class="w3-right">বিস্তারিত</a></li>
		<li>ফিক্সড একাউন্ট <a href="{{ url('mobile/fixed') }}" class="w3-right">বিস্তারিত</a></li>
	</ul>
	@endcan
</div>
@endsection