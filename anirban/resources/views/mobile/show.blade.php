@extends('mobile.layouts.app')

@section('content')
  <div class="w3-responsive">
    <table class="w3-table-all">
      <tr>
        <th>কিস্তি তারিখ</th>
        <th>জমা তারিখ</th>
        <th>সঞ্চয়</th>
        <th>ঋণ জমা</th>
        <th>উত্তোলন</th>
        <th>বোনাস</th>
      </tr>
      @foreach($amounts as $data)
      <tr>
        <td>{{ \App\Classes\Helper::getDate($data->amount_date) }}</td>
        <td>{{ \App\Classes\Helper::getDate($data->created_at) }}</td>
        <td>{{ \App\Classes\Helper::getNumber($data->saving_amount) }}</td>
        <td>{{ \App\Classes\Helper::getNumber($data->lone_amount) }}</td>
        <td>{{ \App\Classes\Helper::getNumber($data->elevation_amount) }}</td>
        <td>{{ \App\Classes\Helper::getNumber($data->bonus_amount) }}</td>
      </tr>
      @endforeach
    </table>
  </div>
@endsection