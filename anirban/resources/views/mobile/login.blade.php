@extends('mobile.layouts.app')

@section('content')
	<form class="w3-container" method="POST" action="{{ url('mobile/login') }}">
    {{ csrf_field() }}
		<p>
		<label class="w3-text-black"><b>সদস‍্য আইডি</b></label>
		<input id="id" type="test" class="w3-input w3-border" name="id" value="{{ old('email') }}"  placeholder="AnirBan">
		</p>
		<p>
		<label class="w3-text-black"><b>পাসওয়ার্ড</b></label>
		<input id="password" type="password" class="w3-input w3-border" name="password" placeholder="Password">
		</p> 
		<p><button class="w3-btn w3-black" type="submit">প্রবেশ</button></p>
	</form>
@endsection