@extends('mobile.layouts.app')

@section('content')
@foreach($lone_accounts as $account)	
<div class="w3-row">
  <div class="w3-col s3">
  	@if(isset($account->user->profile->photo))
    <img src={{ asset($account->user->profile->photo) }} style="width:100%">
    @else
    <img src="img/avatar.png" style="width:100%">
    @endif
    @can('amount.create', Auth::user())
    <span style="display: block; margin-top: 10px">
      <form action="{{ url('sms/sendsms') }}" method="post">
        {{ csrf_field() }}
        <input type="text" name="ac" value="AAN-{{ $account->id }}" hidden>
        <input type="text" name="user_id" value="{{ $account->user->id }}" hidden>
        <input type="text" name="kisty" value="{{ $account->amount_amount_date }}" hidden>
        <input type="text" name="amount" value="{{ ($account->amount_saving_amount + $account->amount_bonus_amount) - $account->amount_elevation_amount}}" hidden>
        <button type="submit" class="w3-button w3-block w3-theme-d3" onclick="return confirm('are you sure?')">SMS</button>
      </form>
    </span>
    <span style="display: block; margin-top: 10px">
      <button class="w3-button w3-block w3-theme-d4">Email</button>
    </span>
    @endcan
  </div>
  <div class="w3-col s9 w3-container">
    <h3>{{ $account->user->name }} <span>{{ \App\Classes\Helper::getLastAmountDate($account->id) }} / AAN-{{ $account->id }}</span></h3>
    <p>ঋণঃ {{ \App\Classes\Helper::getNumber($account->lone_amount) }} টাকা, কিস্তিঃ @if($account->month >= 1) {{ ($account->month) }} @elseif($account->week >= 1) {{ $account->week }} @elseif($account->day >= 1) {{ $account->day }} @else জানা নাই!!! @endif - {{ $account->amount_amount_date }} = {{ ($account->day + $account->week +$account->month) - ($account->amount_amount_date) }}, সঞ্চয়ঃ {{ \App\Classes\Helper::getNumber((($account->amount_saving_amount + $account->amount_bonus_amount)) - $account->amount_elevation_amount) }} টাকা, ঋণ পরিশোধঃ {{ \App\Classes\Helper::getNumber($account->amount_lone_amount) }} টাকা, সর্বমোটঃ {{ \App\Classes\Helper::getNumber(($account->amount_lone_amount + $account->amount_saving_amount + $account->amount_bonus_amount) - $account->amount_elevation_amount) }} টাকা, বাকি আছেঃ {{ \App\Classes\Helper::getNumber($account->lone_amount - (($account->amount_lone_amount + $account->amount_saving_amount + $account->amount_bonus_amount) - $account->amount_elevation_amount))  }} টাকা।</p>
    @can('amount.create', Auth::user())
    <p>
    	<form action="{{ url('lone/amount') }}" method="post">
	        {{ csrf_field() }}
	        <input type="text" value="{{ $account->id }}" name="account_id" hidden>
	        <input type="text" value="{{ \App\Classes\Helper::getNextAmountDate($account->id) }}" name="amount_date" hidden>
	        <input type="text" value="{{ $account->saving_amount }}" name="saving_amount" hidden>
	        <input type="text" value="{{ $account->lone_total_amount }}" name="lone_amount" hidden>
	      <button class="w3-button w3-block w3-theme-d5" type="submit" onclick="return confirm('are you sure?')">জমাঃ {{ $account->lone_total_amount }} + {{ $account->saving_amount }} = {{ $account->lone_total_amount + $account->saving_amount }} </button>
	    </form>
    </p>
    @endcan
  </div>
</div>  
<hr>
@endforeach
@endsection

