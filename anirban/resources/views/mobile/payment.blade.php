@extends('mobile.layouts.app')

@section('content')
  <div class="w3-responsive">
    <table class="w3-table-all">
      <tr>
        <th>একাউন্টস</th>
        <th>জমা তারিখ</th>
        <th>ঋণ জমা</th>
        <th>টাকা</th>
        <th></th>
      </tr>
      @foreach($amounts as $row)
      <tr>
        <td>{{ $row->account->user->name }} (AAN-{{ $row->account_id }})</td>
        <td>{{ \App\classes\Helper::getDate($row->created_at) }}</td>
        <td>{{ \App\classes\Helper::getDate($row->amount_date) }}</td>
        <td>{{ $row->saving_amount + $row->lone_amount }}</td>
        <td>
          <form id="delete-form-{{ $row->id }}" method="post" action="{{ route('amount.destroy',$row->id) }}" style="display: none">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
          </form>
          <a class="btn btn-danger btn-xs" href="" onclick="
          if(confirm('Are you sure, You Want to delete this?'))
              {
                event.preventDefault();
                document.getElementById('delete-form-{{ $row->id }}').submit();
              }
              else{
                event.preventDefault();
              }" ><img src="{{ asset("mobile/img/delete.png") }}" alt="" width="20px;"></a>
        </td>
      </tr>
      @endforeach
    </table>
  </div>
@endsection