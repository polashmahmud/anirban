@extends('layouts.app')

@section('header-script')

@endsection

@section('content')
<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-3">
    <!-- Profile Image -->
    <div class="box">
      <div class="box-body box-profile">
        @if(isset(Auth::user()->profile->photo))
          <img src={{ asset(Auth::user()->profile->photo) }} class="profile-user-img img-responsive img-circle" alt="User profile picture">
        @else
          <img class="profile-user-img img-responsive img-circle" src={{ asset("admin/dist/img/user4-128x128.jpg") }} alt="User profile picture">
        @endif
        
        <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>
        <p class="text-muted text-center">AnirBan - {{ Auth::id() }}</p>

        {{-- <ul class="list-group list-group-unbordered">
          
          <li class="list-group-item">
            <b>সর্বমোট সঞ্চয়</b> <a class="pull-right"></a>
          </li>
          <li class="list-group-item">
            <b>সর্বমোট ঋণ</b> <a class="pull-right">543</a>
          </li>
          <li class="list-group-item">
            <b>সর্বমোট উত্তোলন</b> <a class="pull-right">13,287</a>
          </li>
          <li class="list-group-item">
            <b>সর্বমোট বোনাস</b> <a class="pull-right">13,287</a>
          </li>
          
        </ul> --}}

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Change Password</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" action="{{ url('dashboard/password') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ Auth::id() }}">
        <div class="box-body">
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password">Password</label>
            <input id="password" type="password" class="form-control" name="password"  placeholder="Password">
            @if ($errors->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
          </div>
          <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password">Retype Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Retype Password">
            @if ($errors->has('password_confirmation'))
              <span class="help-block">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
              </span>
          @endif
          </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
          <button type="submit" class="btn btn-default btn-block">Change Your Password</button>
        </div>
      </form>
    </div>
  </div>

  <div class="col-md-9">
    @foreach($accounts as $account)
    <div class="box box-solid">
      <div class="box-header with-border">
        <i class="fa fa-briefcase"></i>

        <h3 class="box-title">হিসাব নংঃ AAN-{{ $account->id }} / 
      {{ \App\Classes\Helper::getDate($account->date_of_account) }} / 
      {{ \App\Classes\Helper::getNumber($account->saving_amount) }}</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-black">
            <div class="inner">
              <h3>@if($account->month >= 1) {{ $account->month }} মাস @elseif($account->week >= 1) {{ $account->week }} সপ্তাহ @elseif($account->day >= 1) {{ $account->day }} দিন @else কোন সময় নিধর্ারিত হয় নি @endif</h3>

              <p>মোট কিস্তি</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-black">
            <div class="inner">
              <h3>{{ \App\Classes\Helper::getNumber($account->amount->sum('saving_amount')) }}</h3>

              <p>সঞ্চয়</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-black">
            <div class="inner">
              <h3>{{ ($account->month + $account->week + $account->day) - ($account->amount->count('amount_date')) }}</h3>

              <p>কিস্তি বাকি আছে</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-black">
            <div class="inner">
              <h3>{{ \App\Classes\Helper::getNumber($account->lone_amount) }}</h3>

              <p>ঋণের পরিমান</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-black">
            <div class="inner">
              <h3>{{ \App\Classes\Helper::getNumber($account->amount->sum('lone_amount')) }}</h3>

              <p>ঋণ জমা</p>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-black">
            <div class="inner">
              <h3>{{ $account->amount->count('amount_date') }}</h3>

              <p>কিস্তি দিয়েছে</p>
            </div>
          </div>
        </div>

      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <a href="{{ route('dashboard.show', $account->id) }}">@if($account->status == 0) একাউন্টটি চালু আছে @else একাউন্টটি বন্ধ আছে@endif <span class="pull-right"><strong>বিস্তারিত দেখুন</strong></span></a>
      </div>
    </div>
    @endforeach
  </div>
</div>
</section>
<!-- /.content -->
@endsection

