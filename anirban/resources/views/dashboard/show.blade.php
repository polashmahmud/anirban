@extends('layouts.app')

@section('header-script')

@endsection

@section('content')
<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-md-3">
    <!-- Profile Image -->
    <div class="box box-primary">
      <div class="box-body box-profile">
        @if(isset(Auth::user()->profile->photo))
          <img src={{ asset(Auth::user()->profile->photo) }} class="profile-user-img img-responsive img-circle" alt="User profile picture">
        @else
          <img class="profile-user-img img-responsive img-circle" src={{ asset("admin/dist/img/user4-128x128.jpg") }} alt="User profile picture">
        @endif
        
        <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

        <p class="text-muted text-center">AnirBan - {{ Auth::id() }}</p>

        {{-- <ul class="list-group list-group-unbordered">
          
          <li class="list-group-item">
            <b>সর্বমোট সঞ্চয়</b> <a class="pull-right"></a>
          </li>
          <li class="list-group-item">
            <b>সর্বমোট ঋণ</b> <a class="pull-right">543</a>
          </li>
          <li class="list-group-item">
            <b>সর্বমোট উত্তোলন</b> <a class="pull-right">13,287</a>
          </li>
          <li class="list-group-item">
            <b>সর্বমোট বোনাস</b> <a class="pull-right">13,287</a>
          </li>
          
        </ul> --}}

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <a href="{{ route('dashboard') }}" class="btn btn-block btn-default" >Back</a>
  </div>

  <div class="col-md-9">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">একাউন্টস সমূহ</h3>

        {{-- <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

            <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div> --}}
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tbody><tr>
            <th>#</th>
            <th>তারিখ</th>
            <th>সঞ্চয়</th>
            <th>ঋণ জমা</th>
            <th>উত্তোলন</th>
            <th>বোনাস</th>
            @can('lone.create', Auth::user())
            <th></th>
            @endcan
          </tr>
          @foreach($account->amount as $data)
          <tr>
            <td>{{ $loop->index + 1 }}</td>
            <td>{{ \App\Classes\Helper::getDate($data->amount_date) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->saving_amount) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->lone_amount) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->elevation_amount) }}</td>
            <td>{{ \App\Classes\Helper::getNumber($data->bonus_amount) }}</td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th></th>
            <th></th>
            <th>{{  $account->amount->sum('saving_amount')  }}</th>
            <th>{{  $account->amount->sum('lone_amount')  }}</th>
            <th>{{  $account->amount->sum('elevation_amount')  }}</th>
            <th>{{  $account->amount->sum('bonus_amount')  }}</th>
            <th></th>
          </tr>
        </tfoot>
      </table>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
</section>
<!-- /.content -->
@endsection

