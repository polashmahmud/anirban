@extends('layouts.app')

@section('title', 'Dashboard')
@section('content-title', 'ডেসবোর্ড')
@section('content-subtitle', 'অনির্বাণ সঞ্চয় ও ঋণদান সমিতি')
@section('link-list')
<li class="active"><a href="#"><i class="fa fa-dashboard"></i> ডেসবোর্ড</a></li>
@endsection

@section('header-script')
<link rel="stylesheet" href={{ asset("admin/bower_components/jvectormap/jquery-jvectormap.css") }}>
<link rel="stylesheet" href={{ asset("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}>
@endsection

@section('content')

@can('dashboard.view')

<!-- Main content -->
<section class="content">
  <!-- Info boxes -->
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-bank"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">সর্বমোট সঞ্চয়</span>
          <span class="info-box-number">{{ \App\Classes\Helper::getNumber($total_saving) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-money"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">ক‍্যাশে আছে</span>
          <span class="info-box-number">{{ \App\classes\Helper::getNumber($balance_amount - ($main_account_balance->amount_saving_amount + $main_account_balance->amount_lone_amount)) }}</span>
          {{-- <span class="info-box-number">{{ \App\Classes\Helper::getNumber($total_cost) }}</span> --}}
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-briefcase"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">অবশিষ্ট</span>
          <span class="info-box-number">{{ \App\Classes\Helper::getNumber($balance_amount) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-envelope-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">SMS Balance</span>
          <span class="info-box-number">{{ \App\Classes\Helper::getNumber($sms_use) }} / {{ str_replace('Your Balance is:SMS ', '', $sms_balance) }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">মাসিক সঞ্চয় এবং ঋণ হিসাব</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-8">
              <p class="text-center">
                <strong>অনির্বাণ শুরুঃ ১৫ আগষ্ট, ২০১৭</strong>
              </p>

              <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="salesChart" style="height: 180px;"></canvas>
              </div>
              <!-- /.chart-responsive -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>লক্ষ‍্য সম্পন্ন</strong>
              </p>

              <div class="progress-group">
                <span class="progress-text">হিসাব</span>
                <span class="progress-number"><b>{{ \App\Classes\Helper::getNumber($total_ac) }}</b>/{{ \App\Classes\Helper::getNumber($user_percentage) }}</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-aqua" style="width: {{ \App\Classes\Helper::getPercentage($total_ac, $user_percentage) }}%"></div>
                </div>
              </div>
              <!-- /.progress-group -->
              <div class="progress-group">
                <span class="progress-text">সঞ্চয়</span>
                <span class="progress-number"><b>{{ \App\Classes\Helper::getNumber($fixed_accounts->sum('amount_saving_amount')) }}</b>/{{ \App\Classes\Helper::getNumber($saving_percentage) }}</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-red" style="width: {{ \App\Classes\Helper::getPercentage($total_saving, $saving_percentage) }}%"></div>
                </div>
              </div>
              <!-- /.progress-group -->
              <div class="progress-group">
                <span class="progress-text">লোন</span>
                <span class="progress-number"><b>{{ \App\Classes\Helper::getNumber($account_lone_amount_c) }}</b>/{{ \App\Classes\Helper::getNumber($lone_percentage) }}</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-green" style="width: {{ \App\Classes\Helper::getPercentage($account_lone_amount_c, $lone_percentage) }}%"></div>
                </div>
              </div>
              <!-- /.progress-group -->
              <div class="progress-group">
                <span class="progress-text">লভ‍্যাংশ</span>
                <span class="progress-number"><b>{{ \App\Classes\Helper::getNumber($others_saving_amount_s - $others_elevation_amount_c) }}</b>/{{ \App\Classes\Helper::getNumber($balance_percentage) }}</span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-yellow" style="width: {{ \App\Classes\Helper::getPercentage(($others_saving_amount_s - $others_elevation_amount_c), $balance_percentage) }}%"></div>
                </div>
              </div>
              <!-- /.progress-group -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- ./box-body -->
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <span class="description-percentage {{ \App\Classes\Helper::getProgressText($total_user, $user_percentage) }}"><i class="fa fa-caret-up"></i> {{ \App\Classes\Helper::getPercentage($total_user, $user_percentage) }}%</span>
                <h5 class="description-header">{{ $total_user }}</h5>
                <span class="description-text">সর্বমোট সদস‍্য</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <span class="description-percentage {{ \App\Classes\Helper::getProgressText($total_saving, $saving_percentage) }}"><i class="fa fa-caret-left"></i> {{ \App\Classes\Helper::getPercentage($total_saving, $saving_percentage) }}%</span>
                <h5 class="description-header">{{ \App\Classes\Helper::getNumber($total_saving) }}</h5>
                <span class="description-text">সর্বমোট সঞ্চয়</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <span class="description-percentage {{ \App\Classes\Helper::getProgressText($account_lone_amount_c, $lone_percentage) }}"><i class="fa fa-caret-up"></i> {{ \App\Classes\Helper::getPercentage($account_lone_amount_c, $lone_percentage) }}%</span>
                <h5 class="description-header">{{ \App\Classes\Helper::getNumber($account_lone_amount_c) }}</h5>
                <span class="description-text">সর্বমোট ঋণ প্রদান</span>
              </div>
              <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-3 col-xs-6">
              <div class="description-block">
                <span class="description-percentage {{ \App\Classes\Helper::getProgressText($balance_amount, $balance_percentage) }}"><i class="fa fa-caret-down"></i> {{ \App\Classes\Helper::getPercentage($balance_amount, $balance_percentage) }}%</span>
                <h5 class="description-header">{{ \App\Classes\Helper::getNumber($balance_amount) }}</h5>
                <span class="description-text">অবশিষ্ট টাকা হাতে আছে</span>
              </div>
              <!-- /.description-block -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-8">
      <!-- MAP & BOX PANE -->
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">ঋণ একাউন্ট সমূহ</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>নাম</th>
                <th>ঋণ নং</th>
                <th>তারিখ</th>
                <th>সঞ্চয়</th>
                <th>ঋণ</th>
                <th>মোট</th>
                <th>কিস্তি</th>
            </tr>
           </thead>
           <tbody>
            <!--<?php $total_saving_amount = 0; ?>-->
            <!--<?php $total_lone_amount = 0; ?>-->
            <!--<?php $total_amount = 0; ?>-->
            <!--<?php $total_lone = 0; ?>-->
            @foreach($lone_accounts as $data)
            <!--<?php $total_saving_amount += (($data->amount_saving_amount + $data->amount_bonus_amount) - $data->amount_elevation_amount); ?>-->
            <!--<?php $total_lone_amount += $data->amount_lone_amount; ?>-->
            <!--<?php $total_amount += ((($data->amount_saving_amount + $data->amount_bonus_amount) - $data->amount_elevation_amount) + $data->amount_lone_amount); ?>-->
            <!--<?php $total_lone += $data->lone_amount; ?>-->
            <tr>
              <td>
                @if(isset($data->user->profile->photo))
                  <img src={{ asset($data->user->profile->photo) }} alt="User Image" class="img-responsive img-circle img-sm">
                  @else
                  <img src={{ asset("admin/dist/img/avatar.png") }} alt="User Image" class="img-responsive img-circle img-sm">
                @endif
                <span style="padding-left: 10px;">
                @can('dashboard.create')<a href="{{ url('member', $data->user->id) }}">@endcan{{ $data->user->name }}@can('dashboard.create')</a>@endcan </span></td>
              <td><a href="{{ route('account.show', $data->id) }}">AAN-{{ $data->id }}</a></td>
              <td>{{ \App\Classes\Helper::getLastAmountDate($data->id) }} <span>({{ \App\Classes\Helper::getDiffInDays($data->date_of_account) }})</span></td>
              <!--<?php $saving_amount = (($data->amount_saving_amount + $data->amount_bonus_amount) - $data->amount_elevation_amount); ?>-->
              {{-- Total Saving --}}
              <td>{{ \App\Classes\Helper::getNumber($saving_amount) }}</td>
              {{-- Total Lone --}}
              <td>{{ \App\Classes\Helper::getNumber($data->amount_lone_amount) }}</td>
              {{-- Total Amount / Total Lone --}}
              <td>{{ \App\Classes\Helper::getNumber((($data->amount_saving_amount + $data->amount_bonus_amount) - $data->amount_elevation_amount) + $data->amount_lone_amount) }} / {{ \App\Classes\Helper::getNumber($data->lone_amount) }}</td>
              <td>{{ $data->day }} - {{ $data->amount_amount_date }} = {{ $data->day - $data->amount_amount_date }}</td>
              {{-- <td>
                @can('dashboard.create')
                <a class="btn btn-default btn-xs" href="{{ url('amount/daly', $data->id) }}"><span class="fa fa-money"></span></a>
                @endcan
              </td> --}}
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
                <th>নাম</th>
                <th>ঋণ নং</th>
                <th>তারিখ</th>
                <th>{{ \App\Classes\Helper::getNumber($total_saving_amount) }}</th>
                <th>{{ \App\Classes\Helper::getNumber($total_lone_amount) }}</th>
                <th>{{ \App\Classes\Helper::getNumber($total_amount) }} / {{ \App\Classes\Helper::getNumber($total_lone) }}</th>
                <th>কিস্তি</th>
            </tr>
          </tfoot>
        </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <!-- TABLE: LATEST ORDERS -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">সঞ্চয় একাউন্ট সমূহ</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example2" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>নাম</th>
                <th>হিসাব নং</th>
                <th>তারিখ</th>
                <th>সঞ্চয়</th>
                <th>কিস্তির হিসাব</th>
              </tr>
          </thead>
          <tbody>
            <!--<?php $total_saving_amount1 = 0; ?>-->
            @foreach($saving_accounts as $data)
            <!--<?php $total_saving_amount1 += ($data->amount_saving_amount + $data->amount_bonus_amount) - $data->amount_elevation_amount; ?>-->
            <tr>
              <td>
                @if(isset($data->user->profile->photo))
                  <img src={{ asset($data->user->profile->photo) }} alt="User Image" class="img-responsive img-circle img-sm">
                  @else
                  <img src={{ asset("admin/dist/img/avatar.png") }} alt="User Image" class="img-responsive img-circle img-sm">
                @endif
                <span style="padding-left: 10px;">
                @can('dashboard.create')<a href="{{ url('member', $data->user->id) }}">@endcan {{ $data->user->name }}@can('dashboard.create')</a>@endcan</span></td>
              <td><a href="{{ route('account.show', $data->id) }}">AAN-{{ $data->id }}</a></td>
              <td>{{ \App\Classes\Helper::getLastAmountDate($data->id) }}</td>
              <td>{{ \App\Classes\Helper::getNumber(($data->amount_saving_amount + $data->amount_bonus_amount) - $data->amount_elevation_amount) }}</td>
              <td>{{ $data->day + $data->week +$data->month }} - {{ $data->amount_amount_date }} = {{ ($data->day + $data->week +$data->month) - ($data->amount_amount_date) }}</td>
              {{-- <td>
                @can('dashboard.create')
                <a class="btn btn-default btn-xs" href="{{ url('amount/saving', $data->id) }}"><span class="fa fa-money"></span></a>
                @endcan
              </td> --}}
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>নাম</th>
              <th>হিসাব নং</th>
              <th>তারিখ</th>
              <th>{{ \App\Classes\Helper::getNumber($total_saving_amount1) }}</th>
              <th>কিস্তির হিসাব</th>
            </tr>
          </tfoot>
        </table>
        </div>
      </div>
      <!-- /.box -->

      <!-- TABLE: LATEST ORDERS -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">ফিক্সড একাউন্ট সমূহ</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example3" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>নাম</th>
                <th>হিসাব নং</th>
                <th>তারিখ</th>
                <th>সঞ্চয়</th>
                <th>কিস্তির হিসাব</th>
              </tr>
          </thead>
          <tbody>
            <!--<?php $total_saving_amount2 = 0; ?>-->
            @foreach($fixed_accounts as $data)
            <!--<?php $total_saving_amount2 += ($data->amount_saving_amount + $data->amount_bonus_amount) - $data->amount_elevation_amount; ?>-->
            <tr>
              <td>
                @if(isset($data->user->profile->photo))
                  <img src={{ asset($data->user->profile->photo) }} alt="User Image" class="img-responsive img-circle img-sm">
                  @else
                  <img src={{ asset("admin/dist/img/avatar.png") }} alt="User Image" class="img-responsive img-circle img-sm">
                @endif
                <span style="padding-left: 10px;">
                @can('dashboard.create')<a href="{{ url('member', $data->user->id) }}">@endcan {{ $data->user->name }}@can('dashboard.create')</a>@endcan</span></td>
              <td><a href="{{ route('account.show', $data->id) }}">AAN-{{ $data->id }}</a></td>
              <td>{{ \App\Classes\Helper::getLastAmountDate($data->id) }}</td>
              <td>{{ \App\Classes\Helper::getNumber(($data->amount_saving_amount + $data->amount_bonus_amount) - $data->amount_elevation_amount) }}</td>
              <td>{{ $data->day + $data->week +$data->month }} - {{ $data->amount_amount_date }} = {{ ($data->day + $data->week +$data->month) - ($data->amount_amount_date) }}</td>
              {{-- <td>
                @can('dashboard.create')
                <a class="btn btn-default btn-xs" href="{{ url('amount/saving', $data->id) }}"><span class="fa fa-money"></span></a>
                @endcan
              </td> --}}
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>নাম</th>
              <th>হিসাব নং</th>
              <th>তারিখ</th>
              <th>{{ \App\Classes\Helper::getNumber($total_saving_amount2) }}</th>
              <th>কিস্তির হিসাব</th>
            </tr>
          </tfoot>
        </table>
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    @can('amount.view')
    @foreach($user_cash_balance as $row)
    <div class="col-md-4">
      <!-- Widget: user widget style 1 -->
      <div class="box box-widget widget-user-2">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-white">
          <div class="widget-user-image">
            <img src={{ asset(\App\classes\Helper::getAvatar($row->user_id)) }} alt="User Avatar" class="img-circle" style="width: 65px; height: 65px;">
          </div>
          <!-- /.widget-user-image -->
          <h3 class="widget-user-username">{{ \App\classes\Helper::getName($row->user_id) }}</h3>
          <h5 class="widget-user-desc">TK: {{ \App\classes\Helper::getNumber($row->amount_saving_amount + $row->amount_lone_amount) }} 
            @if($row->user_id != 1)
            @can('amount.delete', Auth::user())
            <span>
              <form action="{{ url('amount/transfer') }}" method="post">
              {{ csrf_field() }}  
              <input type="text" value="{{ $row->user_id }}" name="user_id" hidden>
              <button type="submit" class="btn btn-default pull-right btn-xs">ট্রান্সফার</button>
              </form>
            </span>
            @endcan
            @endif
          </h5>
        </div>
      </div>
      <!-- /.widget-user -->
    </div>
    @endforeach
    @endcan
    <div class="col-md-4">
      <!-- /.info-box -->
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="fa fa-money"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">সর্বমোট লভ‍্যাংশ / সর্বমোট খরচ / লভ‍্যাংশ</span>
          <span class="info-box-number">{{ \App\Classes\Helper::getNumber($others_saving_amount_s) }} / {{ \App\Classes\Helper::getNumber($others_elevation_amount_c) }} / {{ \App\Classes\Helper::getNumber($others_saving_amount_s - $others_elevation_amount_c) }}</span>

          <div class="progress">
            <div class="progress-bar" style="width: {{ ($others_saving_amount_s / 10000) * 100 }}%"></div>
          </div>
          <span class="progress-description">
                {{ ((\App\Classes\Helper::getDiffInDays('2017-10-15')) * 100) / ($others_saving_amount_s - $others_elevation_amount_c) }}% হয়েছে {{ \App\Classes\Helper::getDiffInDays('2017-10-15') }} দিনে
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
      <div class="info-box bg-blue">
        <span class="info-box-icon"><i class="fa fa-briefcase"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">সদস‍্যদের বোনাস প্রদান / লাভ</span>
          <span class="info-box-number">{{ \App\Classes\Helper::getNumber($amount_bonus_amount_c) }} / {{ \App\Classes\Helper::getNumber($others_saving_amount_s - $others_elevation_amount_c) }} = {{ \App\Classes\Helper::getNumber(($others_saving_amount_s - $others_elevation_amount_c) - $amount_bonus_amount_c) }}</span>

          <div class="progress">
            <div class="progress-bar" style="width: {{ ($amount_bonus_amount_c / ($others_saving_amount_s - $others_elevation_amount_c)) * 100 }}%"></div>
          </div>
          <span class="progress-description">
                {{ ($amount_bonus_amount_c / ($others_saving_amount_s - $others_elevation_amount_c)) * 100 }}% হয়েছে {{ \App\Classes\Helper::getDiffInDays('2017-10-15') }} দিনে
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
      <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-credit-card"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">সর্বমোট টাকা উত্তোলন / মূলধন</span>
          <span class="info-box-number">{{ \App\Classes\Helper::getNumber($amount_elevation_amount_c) }} / {{ \App\Classes\Helper::getNumber($lone_accounts->sum('amount_saving_amount') + $lone_accounts->sum('amount_lone_amount') + $saving_accounts->sum('amount_saving_amount') + $fixed_accounts->sum('amount_saving_amount')) }}</span>

          <div class="progress">
            <div class="progress-bar" style="width: {{ ($amount_elevation_amount_c / ($amount_saving_amount_s + $amount_bonus_amount_c)) * 100 }}%"></div>
          </div>
          <span class="progress-description">
                {{ ($amount_elevation_amount_c / ($amount_saving_amount_s + $amount_bonus_amount_c)) * 100 }}% হয়েছে {{ \App\Classes\Helper::getDiffInDays('2017-10-15') }} দিনে
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
      <!-- /.info-box -->
      {{-- <div class="info-box bg-blue"> --}}
        {{-- <span class="info-box-icon"><i class="fa fa-line-chart"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">লভ‍্যাংশ / মূলধন</span>
          <span class="info-box-number">{{ \App\Classes\Helper::getNumber($balance_amount - $amount_saving_amount_s) }} / {{ \App\Classes\Helper::getNumber($amount_saving_amount_s) }}</span>

          <div class="progress">
            <div class="progress-bar" style="width: {{ (($balance_amount - $amount_saving_amount_s) / 100000) * 100  }}%"></div>
          </div>
          <span class="progress-description">
                {{ (($balance_amount - $amount_saving_amount_s) / 100000) * 100  }}% হয়েছে {{ \App\Classes\Helper::getDiffInDays('2017-10-15') }} দিনে
          </span>
        </div> --}}
        <!-- /.info-box-content -->
      {{-- </div> --}}
      <!-- /.info-box -->
      <!-- /.info-box -->
      <div class="info-box bg-navy">
        <span class="info-box-icon"><i class="fa fa-line-chart"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">আজ টাকা কালেকশন</span>
          <span class="info-box-number">{{ \App\Classes\Helper::getNumber($today_collaction) }} / 5,000</span>

          <div class="progress">
            <div class="progress-bar" style="width: {{ ($today_collaction / 5000) * 100  }}%"></div>
          </div>
          <span class="progress-description">
                {{ ($today_collaction / 5000) * 100  }}% হয়েছে {{ \App\Classes\Helper::getDiffInDays('2017-10-15') }} দিনে
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->

      {{-- <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Browser Usage</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-8">
              <div class="chart-responsive">
                <canvas id="pieChart" height="150"></canvas>
              </div>
              <!-- ./chart-responsive -->
            </div>
            <!-- /.col -->
            <div class="col-md-4">
              <ul class="chart-legend clearfix">
                <li><i class="fa fa-circle-o text-red"></i> Chrome</li>
                <li><i class="fa fa-circle-o text-green"></i> IE</li>
                <li><i class="fa fa-circle-o text-yellow"></i> FireFox</li>
                <li><i class="fa fa-circle-o text-aqua"></i> Safari</li>
                <li><i class="fa fa-circle-o text-light-blue"></i> Opera</li>
                <li><i class="fa fa-circle-o text-gray"></i> Navigator</li>
              </ul>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer no-padding">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="#">United States of America
              <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
            <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
            </li>
            <li><a href="#">China
              <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li>
          </ul>
        </div>
        <!-- /.footer -->
      </div>
      <!-- /.box --> --}}

      <!-- PRODUCT LIST -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">অন‍্যান‍্য খরচ</h3>

          {{-- <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div> --}}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <ul class="products-list product-list-in-box">
            @foreach($others as $other)
            <li class="item">
              <div class="product-img">
                @if($other->elevation_amount > 0)
                <img src={{ asset("img/sad.png") }} alt="Product Image">
                @endif
                @if($other->saving_amount > 0)
                <img src={{ asset("img/happy.png") }} alt="Product Image">
                @endif
              </div>
              <div class="product-info">
                <a href="javascript:void(0)" class="product-title">{{ \App\Classes\Helper::getDiffForHumans($other->other_date) }}
                  @if($other->elevation_amount > 0)
                  <span class="label label-warning pull-right">{{ $other->elevation_amount }}</span>
                  @endif
                  @if($other->saving_amount > 0)
                  <span class="label label-success pull-right" style="margin-right: 5px;">{{ $other->saving_amount }}</span>
                  @endif
                </a>
                <span class="product-description">
                      {{ $other->other_description }}
                </span>
              </div>
            </li>
            @endforeach
            <!-- /.item -->
            
          </ul>
        </div>
        <!-- /.box-body -->
        @can('dashboard.create')
        <div class="box-footer text-center">
          <form action="{{ route('other.store') }}" method="post">
            {{ csrf_field() }}
            <input type="text" hidden="" value="{{ \Carbon\Carbon::today()->toDateString() }}" name="other_date">
              <div class="form-group">
                <label for="other_description">বিস্তারিত</label>
                <input type="text" class="form-control" id="other_description" name="other_description">
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="elevation_amount">খরচ</label>
                    <input type="number" class="form-control" id="elevation_amount" name="elevation_amount">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="saving_amount">জমা</label>
                    <input type="number" class="form-control" id="saving_amount" name="saving_amount">
                  </div>
                </div>
              </div>
              <hr>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-flat btn-block">অন‍্যান‍্য খরচ</button> 
              </div>
          </form>
        </div>
        @endcan
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
      
      <!-- DIRECT CHAT -->
      <div class="box box-warning direct-chat direct-chat-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Task</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <!-- Conversations are loaded here -->
          <div class="direct-chat-messages">
            <!-- Message. Default to the left -->
            <div class="direct-chat-msg">
              @foreach($tasks as $task)
              <div class="direct-chat-info clearfix">
                {{-- <span class="direct-chat-name pull-left">Alexander Pierce</span> --}}
                <span class="direct-chat-timestamp pull-right">
                  @can('dashboard.create') <a href="{{ url('task/user', $task->id) }}"><i class="fa  fa-check"></i></a> @endcan {{ \App\Classes\Helper::getDiffForHumans($task->task_date) }}</span>
              </div>
              <!-- /.direct-chat-info -->
              <img class="direct-chat-img" src={{ asset("img/task.jpg") }} alt="message user image">
              <!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                {{ $task->task_description }}
              </div>
              <!-- /.direct-chat-text -->
              @endforeach
            </div>
            <!-- /.direct-chat-msg -->
          </div>
          <!--/.direct-chat-messages-->

        </div>
        <!-- /.box-body -->
        @can('dashboard.create')
        <div class="box-footer">
          <form action="{{ route('task.store') }}" method="post">
            {{ csrf_field() }}
            <div class="input-group">
              <input type="text" hidden="" value="{{ \Carbon\Carbon::today()->toDateString() }}" name="task_date">
              <input type="number" hidden="" value="0" name="task_amount">
              <input type="text" name="task_description" placeholder="Type Task Description ..." class="form-control">
              <span class="input-group-btn">
                    <button type="submit" class="btn btn-warning btn-flat">Task</button>
                  </span>
            </div>
          </form>
        </div>
        @endcan
        <!-- /.box-footer-->
      </div>
      <!--/.direct-chat -->

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@endcan
@endsection

@section('footer-script')
<script src={{ asset("admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js") }}></script>
<script src={{ asset("admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js") }}></script>
<script src={{ asset("admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js") }}></script>
<script src={{ asset("js/Chart.js") }}></script>
<script src={{ asset("admin/bower_components/datatables.net/js/jquery.dataTables.min.js") }}></script>
<script src={{ asset("admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}></script>
<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>
<script>
  $(function () {
    $('#example2').DataTable()
  })
</script>
<script>
  $(function () {
    $('#example3').DataTable()
  })
</script>
{{-- <script src={{ asset("admin/dist/js/pages/dashboard2.js") }}></script> --}}
<script>
  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  // -----------------------
  // - MONTHLY SALES CHART -
  // -----------------------

  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
  // This will get the first returned node in the jQuery collection.
  var salesChart       = new Chart(salesChartCanvas);

  var salesChartData = {
    labels  : {!! $amount_archives_month !!},
    datasets: [
      {
        label               : 'Electronics',
        fillColor           : 'rgb(210, 214, 222)',
        strokeColor         : 'rgb(210, 214, 222)',
        pointColor          : 'rgb(210, 214, 222)',
        pointStrokeColor    : '#c1c7d1',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgb(220,220,220)',
        data                : {!! $amount_archives_saving !!}
      },
      {
        label               : 'Digital Goods',
        fillColor           : 'rgba(60,141,188,0.9)',
        strokeColor         : 'rgba(60,141,188,0.8)',
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : {!! $amount_archives_lone !!}
      }
    ]
  };

  var salesChartOptions = {
    // Boolean - If we should show the scale at all
    showScale               : true,
    // Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines      : false,
    // String - Colour of the grid lines
    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    // Number - Width of the grid lines
    scaleGridLineWidth      : 1,
    // Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    // Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines  : true,
    // Boolean - Whether the line is curved between points
    bezierCurve             : true,
    // Number - Tension of the bezier curve between points
    bezierCurveTension      : 0.3,
    // Boolean - Whether to show a dot for each point
    pointDot                : false,
    // Number - Radius of each point dot in pixels
    pointDotRadius          : 4,
    // Number - Pixel width of point dot stroke
    pointDotStrokeWidth     : 1,
    // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,
    // Boolean - Whether to show a stroke for datasets
    datasetStroke           : true,
    // Number - Pixel width of dataset stroke
    datasetStrokeWidth      : 2,
    // Boolean - Whether to fill the dataset with a color
    datasetFill             : true,
    // String - A legend template
    legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio     : true,
    // Boolean - whether to make the chart responsive to window resizing
    responsive              : true
  };

  // Create the line chart
  salesChart.Bar(salesChartData, salesChartOptions);
</script>
@endsection