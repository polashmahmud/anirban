<?php

namespace App\Http\Controllers;

use App\Account;
use App\Amount;
use App\Other;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class MobileController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth')->except(['login']);
    }

    public function login(Request $request)
    {
    	$id = $request->id;
    	$password = $request->password;
    	
        if (Auth::attempt(['id' => $id, 'password' => $password, 'active' => 1])) {
		    
		    return redirect('/mobile/dashboard');
		}
		return back();
    }

    public function dashboard()
    {
        $user_cash_balance = Amount::select(
                    DB::raw('amounts.user_id, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as total_count'
                    ))
                 ->groupBy('user_id')
                 ->get();

        // Accounts Table 
        $account_lone_amount_c = Account::sum('lone_amount');
        // Amounts Table
        $amount_saving_amount_s = Amount::sum('saving_amount');
        $amount_lone_amount_s = Amount::sum('lone_amount');
        $amount_elevation_amount_c = Amount::sum('elevation_amount');
        $amount_bonus_amount_c = Amount::sum('bonus_amount');
        // Others Table
        $others_elevation_amount_c = Other::sum('elevation_amount');
        $others_saving_amount_s = Other::sum('saving_amount');

        // Total Cost
        $total_cost = $account_lone_amount_c + $amount_elevation_amount_c + $amount_bonus_amount_c + $others_elevation_amount_c;
        // Total Saving
        $total_saving = $amount_saving_amount_s + $amount_lone_amount_s + $others_saving_amount_s;

        // Balance Amount
        $balance_amount = $total_saving - $total_cost;

        $main_account_balance = Amount::select(
                    DB::raw('amounts.user_id, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as total_count'
                    ))
                 ->where('user_id', '>', 1)
                 ->first();          

        $accounts = Account::where([['user_id', Auth::id()], ['status', 0]])->get();
        return view('mobile.dashboard', compact('accounts', 'user_cash_balance', 'main_account_balance', 'balance_amount'));
    }

    public function lone()
    {
        // Total Lone Account
        $lone_accounts = Account::select(
                    DB::raw('accounts.*, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as amount_amount_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where([
                    ['accounts.status', '=', '0'],
                    ['accounts.account_type', '=', '0'],
                ])->get();        
        return view('mobile.lone', compact('lone_accounts'));
    }

    public function saving()
    {
        // Total Saving Account
        $saving_accounts = Account::select(
                    DB::raw('accounts.*, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as amount_amount_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where([
                    ['accounts.status', '=', '0'],
                    ['accounts.account_type', '=', '1'],
                ])->get(); 

        return view('mobile.saving', compact('saving_accounts'));
    }

    public function fixed()
    {
        // Total Saving Account
        $fixed_accounts = Account::select(
                    DB::raw('accounts.*, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as amount_amount_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where([
                    ['accounts.status', '=', '0'],
                    ['accounts.account_type', '=', '2'],
                ])->get(); 

        return view('mobile.fixed', compact('fixed_accounts'));
    }

    public function show($id)
    {
        $amounts = Amount::where('account_id', $id)->latest()->get();
        return view('mobile.show', compact('amounts'));
    }

    public function payment($id)
    {
        $amounts = Amount::latest()->where('user_id', $id)->take(30)->get();
        return view('mobile.payment', compact('amounts'));
    }
}
