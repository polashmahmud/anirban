<?php

namespace App\Http\Controllers;

use App\Account;
use App\Amount;
use App\Profile;
use App\Sendsms;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;
use MercurySeries\Flashy\Flashy;

class LoneController extends Controller
{
    public function index()
    {
    	// Total Lone Account
        $lone_accounts = Account::select(
                    DB::raw('accounts.*, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as amount_amount_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where([
                    ['accounts.status', '=', '0'],
                    ['accounts.account_type', '=', '0'],
                ])->get();      

    	return view('lone.index', compact('lone_accounts'));
    }

    public function amount(Request $request)
    {
        $request->validate([
            'account_id' => 'required',
            'amount_date' => 'required',
            'saving_amount' => 'required',
            'lone_amount' => 'required',
        ]);

        $amount = new Amount;
        $amount->user_id = Auth::id();
        $amount->account_id = $request->account_id;
        $amount->amount_date = $request->amount_date;
        $amount->saving_amount = $request->saving_amount;
        $amount->lone_amount = $request->lone_amount;
        $amount->save();

        $lone_accounts = Account::select(
                    DB::raw('accounts.*, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as amount_amount_date'
                    ))
                ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                ->where('account_id', $request->account_id)
                ->first();        

        $kisty = $lone_accounts->amount_amount_date;
        $amount = ($lone_accounts->amount_saving_amount + $lone_accounts->amount_lone_amount) - $lone_accounts->amount_elevation_amount;
        $balance = $lone_accounts->lone_amount - $amount;

        $description = 'AAN-'.$lone_accounts->id.', কিস্তি: '.$kisty.' টি, পরিশোধ: '.$amount.', বাকি আছে: '.$balance.' টাকা';

        if (isset($lone_accounts->user->profile->contact_number)) {

            $smsContent = rawurlencode($description);

            $sendsms = new Sendsms;
            $sendsms->user_id = $lone_accounts->user_id;
            $sendsms->account_id = 'AAN-'.$lone_accounts->user_id.'';
            $sendsms->number = $lone_accounts->user->profile->contact_number;
            $sendsms->description = $description;
            $sendsms->save();

            // Curl::to('http://smscp.jadukor.com/smsapi?api_key=C20007375a5e245553ee32.92360903&type=unicode&contacts='.$lone_accounts->user->profile->contact_number.'&senderid=ANIRBAN&msg='.$smsContent.'')->get();
        }

        Flashy::message('সঠিক ভাবে সম্পন্ন হয়েছে', 'http://your-awesome-link.com');
        return back();
    }

    public function fixed(Request $request)
    {
        $request->validate([
            'account_id' => 'required',
            'amount_date' => 'required',
            'saving_amount' => 'required',
            'lone_amount' => 'required',
        ]);

        $amount = new Amount;
        $amount->user_id = Auth::id();
        $amount->account_id = $request->account_id;
        $amount->amount_date = Carbon::now();
        $amount->saving_amount = $request->saving_amount;
        $amount->lone_amount = $request->lone_amount;
        $amount->save();

        Flashy::message('সঠিক ভাবে সম্পন্ন হয়েছে', 'http://your-awesome-link.com');
        return back();
    }
}
