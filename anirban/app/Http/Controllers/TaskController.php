<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        return view('task.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'task_date' => 'required',
            'task_description' => 'required',
            'task_amount' => 'required',
        ]);

        $task = new Task;
        $task->task_date = $request->task_date;
        $task->task_description = $request->task_description;
        $task->task_amount = $request->task_amount;
        $task->status = 0;
        $task->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        return view('task.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $request->validate([
            'task_date' => 'required',
            'task_description' => 'required',
            'task_amount' => 'required',
        ]);

        $task->task_date = $request->task_date;
        $task->task_description = $request->task_description;
        $task->task_amount = $request->task_amount;
        $task->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return redirect()->back();
    }

    public function active(Task $task)
    {
        if($task->status == 1){ $status = 0; }
        if($task->status == 0){ $status = 1; }

        $task->status = $status;
        $task->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return redirect()->back();
    }
}
