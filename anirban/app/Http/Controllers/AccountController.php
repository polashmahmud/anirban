<?php

namespace App\Http\Controllers;

use App\Account;
use App\Amount;
use App\User;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::all();
        return view('account.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('account.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date_of_account' => 'required',
            'saving_amount' => 'required',
            'account_type' => 'required',
        ]);

        $account = new Account;
        $account->user_id = $request->user_id;
        $account->date_of_account = $request->date_of_account;
        $account->day = $request->day;
        $account->month = $request->month;
        $account->week = $request->week;
        $account->lone_amount = $request->lone_amount;
        $account->lone_total_amount = $request->lone_total_amount;
        $account->saving_amount = $request->saving_amount;
        $account->account_type = $request->account_type;
        
        $account->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        $amounts = Amount::where('account_id', $account->id)->latest()->paginate(30);
        return view('account.view', compact('account', 'amounts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        return view('account.edit', compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        $request->validate([
            'date_of_account' => 'required',
            'saving_amount' => 'required',
            'account_type' => 'required',
        ]);

        $account->date_of_account = $request->date_of_account;
        $account->day = $request->day;
        $account->month = $request->month;
        $account->week = $request->week;
        $account->lone_amount = $request->lone_amount;
        $account->lone_total_amount = $request->lone_total_amount;
        $account->saving_amount = $request->saving_amount;
        $account->account_type = $request->account_type;
        
        $account->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        $account->delete();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    public function active(Account $account)
    {
        if($account->status == 1){ $status = 0; }
        if($account->status == 0){ $status = 1; }

        $account->status = $status;
        $account->save();
        
        Flashy::message('Success', 'http://your-awesome-link.com');
        return redirect()->back();
    }
}
