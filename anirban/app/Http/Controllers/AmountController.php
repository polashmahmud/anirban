<?php

namespace App\Http\Controllers;

use App\Account;
use App\Amount;
use App\Other;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use MercurySeries\Flashy\Flashy;

class AmountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $amounts = Amount::all();
        return view('amount.index', compact('amounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accounts = Account::all();
        return view('amount.create', compact('accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'account_id' => 'required',
            'amount_date' => 'required',
            'saving_amount' => 'required',
            'lone_amount' => 'required',
            'elevation_amount' => 'required',
            'bonus_amount' => 'required',
        ]);

        $amount = new Amount;
        $amount->account_id = $request->account_id;
        $amount->amount_date = $request->amount_date;
        $amount->saving_amount = $request->saving_amount;
        $amount->lone_amount = $request->lone_amount;
        $amount->elevation_amount = $request->elevation_amount;
        $amount->bonus_amount = $request->bonus_amount;
        $amount->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Amount  $amount
     * @return \Illuminate\Http\Response
     */
    public function show(Amount $amount)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Amount  $amount
     * @return \Illuminate\Http\Response
     */
    public function edit(Amount $amount)
    {
        return view('amount.edit', compact('amount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Amount  $amount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Amount $amount)
    {
        $request->validate([
            'amount_date' => 'required',
            'saving_amount' => 'required',
            'lone_amount' => 'required',
            'elevation_amount' => 'required',
            'bonus_amount' => 'required',
        ]);

        $amount->amount_date = $request->amount_date;
        $amount->saving_amount = $request->saving_amount;
        $amount->lone_amount = $request->lone_amount;
        $amount->elevation_amount = $request->elevation_amount;
        $amount->bonus_amount = $request->bonus_amount;
        $amount->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Amount  $amount
     * @return \Illuminate\Http\Response
     */
    public function destroy(Amount $amount)
    {
        $amount->delete();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    public function amount(Account $account)
    {
        return view('amount.daly', compact('account'));
    }

    public function saving(Account $account)
    {
        return view('amount.saving', compact('account'));
    }

    public function dayamount(Request $request)
    {
        $request->validate([
            'account_id' => 'required',
            'amount_date' => 'required',
            'saving_amount' => 'required',
            'lone_amount' => 'required',
            'elevation_amount' => 'required',
            'bonus_amount' => 'required',
        ]);

        $dates = explode(',',$request->input('amount_date'));

        foreach($dates as $date){
            $amount = new Amount;
            $amount->account_id = $request->account_id;
            $amount->amount_date = $date;
            $amount->saving_amount = $request->saving_amount;
            $amount->lone_amount = $request->lone_amount;
            $amount->elevation_amount = $request->elevation_amount;
            $amount->bonus_amount = $request->bonus_amount;
            $amount->save();

        }

        Flashy::message('Success', 'http://your-awesome-link.com');
        return redirect()->route('home');
        
    }

    public function amountdetails($id)
    {
        $amounts = Amount::latest()->where('account_id', $id)->get();
        return view('amount.details', compact('amounts'));
    }

    public function transfer(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
        ]);

        Amount::where('user_id', $request->user_id)
          ->update(['user_id' => 1]);

        Flashy::message('Amount Transfer Successfull', 'http://your-awesome-link.com');
        return back();
    }

    public function dashboard()
    {
        // Accounts Table 
        $account_lone_amount_c = Account::sum('lone_amount');
        // Amounts Table
        $amount_saving_amount_s = Amount::sum('saving_amount');
        $amount_lone_amount_s = Amount::sum('lone_amount');
        $amount_elevation_amount_c = Amount::sum('elevation_amount');
        $amount_bonus_amount_c = Amount::sum('bonus_amount');
        // Others Table
        $others_elevation_amount_c = Other::sum('elevation_amount');
        $others_saving_amount_s = Other::sum('saving_amount');

        // Total Cost
        $total_cost = $account_lone_amount_c + $amount_elevation_amount_c + $amount_bonus_amount_c + $others_elevation_amount_c;
        // Total Saving
        $total_saving = $amount_saving_amount_s + $amount_lone_amount_s + $others_saving_amount_s;

        // Balance Amount
        $balance_amount = $total_saving - $total_cost;

        // Today Amount
        $today_amount = Amount::whereDate('created_at', '=', \Carbon\Carbon::today()->toDateString())->get();
        $yesterday_amount = Amount::whereDate('created_at', '=', \Carbon\Carbon::yesterday()->toDateString())->get();
        $all_amount = Amount::latest()->paginate(15);

        $user_cash_balance = Amount::select(
                    DB::raw('amounts.user_id, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as total_count'
                    ))
                 ->groupBy('user_id')
                 ->get(); 

        $main_account_balance = Amount::select(
                    DB::raw('amounts.user_id, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as total_count'
                    ))
                 ->where('user_id', '>', 1)
                 ->first();    

        return view('amount.dashboard', compact('balance_amount', 'today_amount', 'user_cash_balance', 'yesterday_amount', 'main_account_balance','all_amount'));
    }
}
