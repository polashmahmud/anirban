<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MercurySeries\Flashy\Flashy;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
        ]);

        $role = new Role;
        $role->name = $request->name;
        $role->save();

        Flashy::message('New Role Create!', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $role->name = $request->name;
        $role->save();

        Flashy::message('Role Edit Completed!', 'http://your-awesome-link.com');
        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();

        Flashy::message('Role Delete Completed!', 'http://your-awesome-link.com');
        return redirect()->route('role.index');
    }

    public function add()
    {
        $roles = Role::all();
        $permissions = Permission::all();
        return view('role.add', compact('roles', 'permissions'));
    }

    public function savepermission(Request $request)
    {
        $input = $request->all();
        $permissions = array_get($input, 'permission');

        DB::table('permission_role')->truncate();

        if($permissions != '')
        foreach($permissions as $r_key => $permission){
            foreach($permission as $p_key => $per){
                $values[] = $p_key;
            }

            $role = Role::find($r_key);
            
            if(count($values))
            $role->permissions()->attach($values);
            unset($values);
        }

        
        Flashy::message('Success!', 'http://your-awesome-link.com');
        return back();
    }
}
