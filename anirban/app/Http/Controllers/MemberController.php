<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('member.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->active = 0;
        $user->password = bcrypt($request->password);
        $user->save();

        $user->roles()->sync(4);

        $profile = new Profile;
        $profile->user()->associate($user);
        $profile->anirban_code = 'anirban_'.$user->id;
        $profile->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::where('user_id', $id)->first();

        return view('member.show', compact('profile'));
    }

    public function profile(Request $request, Profile $profile)
    {
        if ($request->hasFile('photo')) {
            $imageName = $request->photo->store('public/users');
            $profile->photo = $imageName;
        }
        $profile->date_of_birth = $request->date_of_birth;
        $profile->father_name = $request->father_name;
        $profile->mother_name = $request->mother_name;
        $profile->date_of_joining = $request->date_of_joining;
        $profile->contact_number = $request->contact_number;
        $profile->alternate_contact_number = $request->alternate_contact_number;
        $profile->present_address = $request->present_address;
        $profile->permanent_address = $request->permanent_address;
        $profile->facebook_link = $request->facebook_link;
        $profile->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        return view('member.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->roles()->sync($request->role);
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    public function active(User $user)
    {
        if($user->active == 1){ $active = 0; }
        if($user->active == 0){ $active = 1; }

        $user->active = $active;
        $user->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return redirect()->back();
    }
}
