<?php

namespace App\Http\Controllers;

use App\Other;
use Illuminate\Http\Request;
use MercurySeries\Flashy\Flashy;

class OtherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $others = Other::all();
        return view('other.index', compact('others'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('other.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'other_date' => 'required',
            'other_description' => 'required',
            'elevation_amount' => 'required',
            'saving_amount' => 'required',
        ]);

        $other = new Other;
        $other->other_date = $request->other_date;
        $other->other_description = $request->other_description;
        $other->elevation_amount = $request->elevation_amount;
        $other->saving_amount = $request->saving_amount;
        $other->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Other  $other
     * @return \Illuminate\Http\Response
     */
    public function show(Other $other)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Other  $other
     * @return \Illuminate\Http\Response
     */
    public function edit(Other $other)
    {
        return view('other.edit', compact('other'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Other  $other
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Other $other)
    {
        $request->validate([
            'other_date' => 'required',
            'other_description' => 'required',
            'elevation_amount' => 'required',
            'saving_amount' => 'required',
        ]);

        $other->other_date = $request->other_date;
        $other->other_description = $request->other_description;
        $other->elevation_amount = $request->elevation_amount;
        $other->saving_amount = $request->saving_amount;
        $other->save();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Other  $other
     * @return \Illuminate\Http\Response
     */
    public function destroy(Other $other)
    {
        $other->delete();
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }
}
