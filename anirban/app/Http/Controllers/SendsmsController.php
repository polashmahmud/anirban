<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Sendsms;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use MercurySeries\Flashy\Flashy;

class SendsmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendsms(Request $request)
    {
        $request->validate([
            'ac' => 'required',
            'user_id' => 'required',
            'kisty' => 'required',
            'amount' => 'required',
        ]);

        $number = Profile::where('user_id', $request->user_id)->first();

        if (isset($number->contact_number)) {
            $description = ''.$request->ac.', কিস্তি: '.$request->kisty.' টি, সঞ্চয়: '.$request->amount.' টাকা (অনির্বাণ সমিতি)';
            $smsContent = rawurlencode($description);

            $sendsms = new Sendsms;
            $sendsms->user_id = $request->user_id;
            $sendsms->account_id = $request->ac;
            $sendsms->number = $number->contact_number;
            $sendsms->description = $description;
            $sendsms->save();

            Curl::to('http://smscp.jadukor.com/smsapi?api_key=C20007375a5e245553ee32.92360903&type=unicode&contacts='.$number->contact_number.'&senderid=ANIRBAN&msg='.$smsContent.'')
            ->get();

            Flashy::message('সঠিক ভাবে সম্পন্ন হয়েছে', 'http://your-awesome-link.com');
            return back();
        } else {
            Flashy::error('ফোন নম্বর দেওয়া নাই', 'http://your-awesome-link.com');
            return back();
        }
        
    }

    public function sendlonesms(Request $request)
    {
        $request->validate([
            'ac' => 'required',
            'user_id' => 'required',
            'kisty' => 'required',
            'amount' => 'required',
            'balance' => 'required',
        ]);

        $number = Profile::where('user_id', $request->user_id)->first();

        if (isset($number->contact_number)) {
            $description = ''.$request->ac.', কিস্তি: '.$request->kisty.' টি, পরিশোধ: '.$request->amount.', বাকি আছে: '.$request->balance.' টাকা';
            $smsContent = rawurlencode($description);

            $sendsms = new Sendsms;
            $sendsms->user_id = $request->user_id;
            $sendsms->account_id = $request->ac;
            $sendsms->number = $number->contact_number;
            $sendsms->description = $description;
            $sendsms->save();

            Curl::to('http://smscp.jadukor.com/smsapi?api_key=C20007375a5e245553ee32.92360903&type=unicode&contacts='.$number->contact_number.'&senderid=ANIRBAN&msg='.$smsContent.'')
            ->get();

            Flashy::message('সঠিক ভাবে সম্পন্ন হয়েছে', 'http://your-awesome-link.com');
            return back();
        } else {
            Flashy::error('ফোন নম্বর দেওয়া নাই', 'http://your-awesome-link.com');
            return back();
        }
        
    }
}
