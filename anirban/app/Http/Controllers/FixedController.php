<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FixedController extends Controller
{
    public function index()
    {
    	// Total Saving Account
        $fixed_accounts = Account::select(
                    DB::raw('accounts.*, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as amount_amount_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where([
                    ['accounts.status', '=', '0'],
                    ['accounts.account_type', '=', '2'],
                ])->get(); 

    	return view('fixed.index', compact('fixed_accounts'));
    }
}
