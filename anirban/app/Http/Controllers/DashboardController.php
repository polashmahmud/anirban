<?php

namespace App\Http\Controllers;

use App\Account;
use App\Amount;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use MercurySeries\Flashy\Flashy;

class DashboardController extends Controller
{
    public function index()
    {
    	$accounts = Account::where([['user_id', Auth::id()], ['status', 0]])->get();
    	return view('dashboard.user', compact('accounts'));
    }

    public function show(Account $account)
    {
    	return view('dashboard.show', compact('account'));
    }

    public function password(Request $request)
    {
    	$request->validate([
            'id' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $password = User::find($request->id);
        $password->password = bcrypt($request->password);
        $password->save();
        
        Flashy::message('Success', 'http://your-awesome-link.com');
        return back();
    }
    
}
