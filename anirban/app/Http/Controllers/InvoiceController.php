<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function accountinfo(Account $account)
    {
    	return view('invoice.account-info', compact('account'));
    }

    public function accountinfoprint(Account $account)
    {
    	return view('invoice.account-info-print', compact('account'));
    }

    public function accountform(Account $account)
    {
    	return view('invoice.account-form', compact('account'));
    }
}
