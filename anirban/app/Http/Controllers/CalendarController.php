<?php

namespace App\Http\Controllers;

use App\Account;
use App\Amount;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CalendarController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$amounts = Amount::groupBy(DB::raw('Date(created_at)'))
   				->selectRaw('created_at, updated_at, amount_date, 
   					sum(saving_amount) as saving_amount,
   					sum(lone_amount) as lone_amount,
   					sum(elevation_amount) as elevation_amount,
   					sum(bonus_amount) as bonus_amount')
   				->get();

   		$accounts = Account::groupBy(DB::raw('Date(created_at)'))
   				->selectRaw('created_at, updated_at, 
   					count(id) as count')
   				->get();

   		$users = User::groupBy(DB::raw('Date(created_at)'))
   				->selectRaw('created_at, updated_at, 
   					count(id) as user')
   				->get();	


    	$events = array();

	    	foreach($users as $row){
	            $start = date('Y').'-'.date('m-d',strtotime($row->created_at));
	            $title = 'সদস‍্যঃ ' . $row->user;
	            $color = '#3c8dbc';
	            $events[] = array('title' => $title, 'start' => $start, 'color' => $color);
	        }

	        foreach($accounts as $row){
	            $start = date('Y').'-'.date('m-d',strtotime($row->created_at));
	            $title = 'একাউন্টঃ ' . $row->count;
	            $color = '#00a65a';
	            $events[] = array('title' => $title, 'start' => $start, 'color' => $color);
	        }

	        foreach($amounts as $row){
	            $start = date('Y').'-'.date('m-d',strtotime($row->created_at));
	            $title = 'জমাঃ ' . $row->saving_amount;
	            $color = '#001F3F';
	            $events[] = array('title' => $title, 'start' => $start, 'color' => $color);
	        }

	        foreach($amounts as $row){
	            $start = date('Y').'-'.date('m-d',strtotime($row->created_at));
	            $title = 'ঋণঃ ' . $row->lone_amount;
	            $color = '#D81B60';
	            $events[] = array('title' => $title, 'start' => $start, 'color' => $color);
	        }

	        foreach($amounts as $row){
	            $start = date('Y').'-'.date('m-d',strtotime($row->created_at));
	            $title = 'উত্তোলনঃ ' . $row->elevation_amount;
	            $color = '#ff851b';
	            $events[] = array('title' => $title, 'start' => $start, 'color' => $color);
	        }

	        foreach($amounts as $row){
	            $start = date('Y').'-'.date('m-d',strtotime($row->created_at));
	            $title = 'বোনাসঃ ' . $row->bonus_amount;
	            $color = '#605ca8';
	            $events[] = array('title' => $title, 'start' => $start, 'color' => $color);
	        }

    	return view('calendar.index', compact('events'));
    }
}
