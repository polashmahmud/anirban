<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MercurySeries\Flashy\Flashy;

class SigninController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest');
    }

    public function signin(Request $request)
    {
    	$email = $request->email;
    	$password = $request->password;
    	
        if (Auth::attempt(['email' => $email, 'password' => $password, 'active' => 1])) {
		    // foreach(Auth::user()->roles as $role) {
		    // 	if($role->name == 'admin') {
		    // 		return redirect('')->url('dashboard');
		    // 	} elseif ($role->name == 'moderator') {
		    // 		return redirect()->url('dashboard');
		    // 	} elseif ($role->name == 'editor') {
		    // 		return redirect()->url('dashboard');
		    // 	} else {
		    // 		return redirect('/');
		    // 	}

		    // }
		    return redirect('/dashboard');
		}
		Flashy::error('আপনার ইউজারনেম অথবা পাসওয়ার্ড ভুল আছে', 'http://your-awesome-link.com');
		return redirect()->back();
    }
}
