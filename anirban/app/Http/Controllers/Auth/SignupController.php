<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Profile;
use App\User;
use Illuminate\Http\Request;

class SignupController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest');
    }

    
    public function signup(Request $request)
    {
    	
    	$request->validate([
	        'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
	    ]);

	    $user = new User;
	    $user->name = $request->name;
	    $user->email = $request->email;
	    $user->active = 0;
	    $user->password = bcrypt($request->password);
	    $user->save();

        $profile = new Profile;
        $profile->user()->associate($user);
        $profile->anirban_code = 'anirban_'.$user->id;
        $profile->save();

        return back()->with('success','আপনার রেজিস্ট্রেশন সম্পন্ন হয়েছে কিন্তু আপনার একাউন্টটি ডিএকটিভ আছে। দয়া করে এডমিনের সাথে যোগাযোগ করুন!');
    }
}
