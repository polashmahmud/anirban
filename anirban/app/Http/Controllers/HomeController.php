<?php

namespace App\Http\Controllers;

use App\Account;
use App\Amount;
use App\Other;
use App\Sendsms;
use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;

class HomeController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Total Lone Account
        $lone_accounts = Account::select(
                    DB::raw('accounts.*, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as amount_amount_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where([
                    ['accounts.status', '=', '0'],
                    ['accounts.account_type', '=', '0'],
                ])->get();

        // Total Saving Account
        $saving_accounts = Account::select(
                    DB::raw('accounts.*, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as amount_amount_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where([
                    ['accounts.status', '=', '0'],
                    ['accounts.account_type', '=', '1'],
                ])->get(); 

        // Total Saving Account
        $fixed_accounts = Account::select(
                    DB::raw('accounts.*, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as amount_amount_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where([
                    ['accounts.status', '=', '0'],
                    ['accounts.account_type', '=', '2'],
                ])->get();          

        // All Task              
        $tasks = Task::where('status', '0')->get();

        // All Users
        $users = User::where('active', '1')->take(8)->get();

         // All Others
        $others = Other::take(5)->latest()->get();

        // Total Users
        $total_user = User::where('active', '1')->count('id');
        // Total A/C
        $total_ac = Account::count('id');

        // Monthly Chart
        $monthly_report = Amount::selectRaw(
                            'year(amount_date) year, 
                            monthname(amount_date) month, 
                            sum(saving_amount) saving_amount,
                            sum(lone_amount) lone_amount,
                            sum(elevation_amount) elevation_amount,
                            sum(bonus_amount) bonus_amount,
                            sum(lone_amount) + sum(elevation_amount) + sum(bonus_amount) e_total'
                        )->groupBy('year','month')
                        ->get();

        $amount_archives_month = $monthly_report->pluck('month');                
        $amount_archives_saving = $monthly_report->pluck('saving_amount');                
        $amount_archives_lone = $monthly_report->pluck('e_total');  

        // Accounts Table 
        $account_lone_amount_c = Account::sum('lone_amount');
        // Amounts Table
        $amount_saving_amount_s = Amount::sum('saving_amount');
        $amount_lone_amount_s = Amount::sum('lone_amount');
        $amount_elevation_amount_c = Amount::sum('elevation_amount');
        $amount_bonus_amount_c = Amount::sum('bonus_amount');
        // Others Table
        $others_elevation_amount_c = Other::sum('elevation_amount');
        $others_saving_amount_s = Other::sum('saving_amount');

        // Total Cost
        $total_cost = $account_lone_amount_c + $amount_elevation_amount_c + $amount_bonus_amount_c + $others_elevation_amount_c;
        // Total Saving
        $total_saving = $amount_saving_amount_s + $amount_lone_amount_s + $others_saving_amount_s;

        // Balance Amount
        $balance_amount = $total_saving - $total_cost;

        // Percentage
        $user_percentage = 100;
        $saving_percentage = 300000;
        $lone_percentage = 200000;
        $balance_percentage = 150000;

        // Today Money Collaction
        $today_amount = Amount::selectRaw(
                            'sum(saving_amount) saving_amount,
                            sum(lone_amount) lone_amount,
                            sum(elevation_amount) elevation_amount,
                            sum(bonus_amount) bonus_amount'
                        )->where('amount_date', \Carbon\Carbon::today())->first();
        $today_collaction = $today_amount->saving_amount + $today_amount->lone_amount;
        $today_cost = $today_amount->elevation_amount + $today_amount->bonus_amount;

        $main_account_balance = Amount::select(
                    DB::raw('amounts.user_id, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as total_count'
                    ))
                 ->where('user_id', '>', 1)
                 ->first();

        $user_cash_balance = Amount::select(
                    DB::raw('amounts.user_id, 
                        SUM(amounts.saving_amount) as amount_saving_amount,
                        SUM(amounts.lone_amount) as amount_lone_amount,
                        SUM(amounts.elevation_amount) as amount_elevation_amount,
                        SUM(amounts.bonus_amount) as amount_bonus_amount,
                        Count(amounts.amount_date) as total_count'
                    ))
                 ->groupBy('user_id')
                 ->get();  

        $sms_use = Sendsms::count('id');
        $sms_balance = Curl::to('http://smscp.jadukor.com/miscapi/C20007375a5e245553ee32.92360903/getBalance')->get();


        return view('home', compact('lone_accounts', 'saving_accounts', 'tasks', 'users', 'amount_archives_month', 'amount_archives_saving', 'amount_archives_lone', 'others', 'total_saving', 'total_cost', 'balance_amount', 'account_lone_amount_c', 'total_user', 'user_percentage', 'saving_percentage', 'lone_percentage', 'balance_percentage', 'others_elevation_amount_c', 'others_saving_amount_s', 'amount_bonus_amount_c', 'amount_elevation_amount_c','amount_saving_amount_s', 'today_collaction', 'fixed_accounts', 'total_ac', 'main_account_balance', 'user_cash_balance', 'sms_use', 'sms_balance'));
    }
}
