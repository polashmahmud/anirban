<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function amount()
    {
        return $this->hasMany('App\Amount');
    }
}
