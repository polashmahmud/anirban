<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amount extends Model
{
    public function account()
    {
        return $this->belongsTo('App\Account');
    }
}
