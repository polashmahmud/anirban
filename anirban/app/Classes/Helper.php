<?php 
namespace App\Classes;

use App\Account;
use App\Amount;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\DB;

Class Helper{

	public static function getNumber($id)
	{
		$getNumber = number_format($id);
		return $getNumber;
	}

	public static function getPercentage($number, $percentage)
	{
		$getPercentage = number_format((($number * 100) / $percentage));
		return $getPercentage;
	}

	public static function getDate($data)
	{
		$getDate = \Carbon\Carbon::parse($data)->toFormattedDateString();
		return $getDate;
	}

	public static function getDiffForHumans($data)
	{
		$getDate = \Carbon\Carbon::parse($data)->diffForHumans();
		return $getDate;
	}

	public static function getDiffInDays($data)
	{
		$getDate = \Carbon\Carbon::parse($data)->diffInDays();
		return $getDate;
	}

	public static function getName($id)
	{
		$user = User::find($id);
		$getName = $user->name;
		return $getName;
	}

	public static function getAnirbanCode($id)
	{
		$user = User::find($id);
		$getAnirbanCode = $user->profile->anirban_code;
		return $getAnirbanCode;
	}

	public static function lonePercentage($id)
	{
		$data = Account::select(
                    DB::raw('accounts.day, accounts.week, accounts.month,
                        Count(amounts.amount_date) as done_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where('accounts.id', $id)->first();

        $percentage = ($data->done_date / ($data->day + $data->week + $data->month)) * 100;

        return $percentage;

	}

	public static function getProgress($id)
	{
		$data = Account::select(
                    DB::raw('accounts.day, accounts.week, accounts.month,
                        Count(amounts.amount_date) as done_date'
                    ))
                 ->leftJoin('amounts', 'amounts.account_id', '=', 'accounts.id')
                 ->groupBy('accounts.id')
                 ->where('accounts.id', $id)->first();

        $percentage = ($data->done_date / ($data->day + $data->week + $data->month)) * 100;

        if($percentage <= 10)
			echo 'bg-white-active';
		elseif($percentage>10  && $percentage <=20)
			echo 'bg-maroon-active';
		elseif($percentage>20  && $percentage <=30)
			echo 'bg-orange-active';
		elseif($percentage>30  && $percentage <=40)
			echo 'bg-red-active';
		elseif($percentage>40  && $percentage <=50)
			echo 'bg-yellow-active';
		elseif($percentage>50  && $percentage <=60)
			echo 'bg-purple-active';
		elseif($percentage>60  && $percentage <=70)
			echo 'bg-teal-active';
		elseif($percentage>70  && $percentage <=80)
			echo 'bg-navy-active';
		elseif($percentage>80  && $percentage <=90)
			echo 'bg-green-active';
		else
			echo 'bg-blue-active';
        
	}

	public static function getProgressText($number, $percentage)
	{
		$getProgressText = number_format((($number * 100) / $percentage));

        if($getProgressText <= 10)
			echo 'text-black';
		elseif($getProgressText>10  && $getProgressText <=20)
			echo 'text-red';
		elseif($getProgressText>20  && $getProgressText <=30)
			echo 'text-orange';
		elseif($getProgressText>30  && $getProgressText <=40)
			echo 'text-blue';
		elseif($getProgressText>40  && $getProgressText <=50)
			echo 'text-yellow';
		elseif($getProgressText>50  && $getProgressText <=60)
			echo 'text-purple';
		elseif($getProgressText>60  && $getProgressText <=70)
			echo 'text-teal';
		elseif($getProgressText>70  && $getProgressText <=90)
			echo 'text-green';
		else
			echo 'text-blue';
        
	}

	public static function getPermission($user, $p_id)
    {
        foreach ($user->roles as $role) {
            foreach ($role->permissions as $permission) {
                if($permission->id == $p_id) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function getLastAmountDate($id)
    {
    	$date = Amount::where('account_id', $id)->get()->last();
    	if ($date == null) {
    		$date = Account::where('id', $id)->get()->last();
    		$getDate = \Carbon\Carbon::parse($date->date_of_account)->toFormattedDateString();
			return $getDate;
    	}
    	$getDate = \Carbon\Carbon::parse($date->amount_date)->toFormattedDateString();
		return $getDate;
    }

    public static function getNextAmountDate($id)
    {
    	$date = Amount::where('account_id', $id)->get()->last();
    	if ($date == null) {
    		$date = Account::where('id', $id)->get()->last();
    		$getDate = date('Y-m-d', strtotime($date->date_of_account . ' +1 day'));
			return $getDate;
    	}
    	// $getDate = \Carbon\Carbon::parse($date->amount_date)->addDay();
    	// $getDate = $date->amount_date;
    	$getDate = date('Y-m-d', strtotime($date->amount_date . ' +1 day'));
		return $getDate;
    }

    public static function getAvatar($id)
    {
    	$avatar = Profile::where('user_id', $id)->first();
    	return $avatar->photo;
    }


}

