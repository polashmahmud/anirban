<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::resource('accounts', 'App\Policies\AccountPolicy');
        Gate::resource('dashboard', 'App\Policies\DeshboardPolicy');
        Gate::resource('lone', 'App\Policies\LonePolicy');
        Gate::resource('amount', 'App\Policies\MoneyPolicy');
        Gate::resource('other', 'App\Policies\OtherPolicy');
        Gate::resource('role', 'App\Policies\RolePolicy');
        Gate::resource('task', 'App\Policies\TaskPolicy');
        Gate::resource('users', 'App\Policies\UserPolicy');
    }
}
