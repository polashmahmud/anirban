<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->date('date_of_account');
            $table->string('day', 5)->default(0);
            $table->string('month', 5)->default(0);
            $table->string('week', 5)->default(0);
            $table->float('lone_amount', 8, 2)->default(0);
            $table->float('lone_total_amount', 8, 2)->default(0);
            $table->float('saving_amount', 8, 2)->default(0);
            $table->boolean('account_type')->default(0);
            $table->boolean('status')->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
